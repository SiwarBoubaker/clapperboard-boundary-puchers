package managedBeans_Controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.esprit.pfa.cineast.entities.Comment;
import edu.esprit.pfa.cineast.services.imp.commentDAORemote;


@ManagedBean(name="commbean")
@SessionScoped
public class commentBean {

	@EJB
	private commentDAORemote commentejb; 
	
	private List<Comment> comments; 
	

	@PostConstruct
	public void init()
	{
		setComments(commentejb.retrieveAll()); 
		
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	

}

package managedBeans_Controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import edu.esprit.pfa.cineast.entities.Comment;
import edu.esprit.pfa.cineast.services.imp.commentDAORemote;

@ManagedBean(name="commentbeans")
@SessionScoped
public class CommentpostBean {

	@EJB
	private commentDAORemote commentejb;
	
	private Comment comm= new Comment(); 
	
	public void doAddcomment()
	{
		commentejb.create(comm); 
	}

	public Comment getComm() {
		return comm;
	}

	public void setComm(Comment comm) {
		this.comm = comm;
	}

	
}

package managedBeans_Controllers;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import edu.esprit.pfa.cineast.entities.Forum;
import edu.esprit.pfa.cineast.services.imp.ForumDAOLocal;



@ManagedBean(name="editorbeancreat")
@SessionScoped
public class EditorBeanCreat {

	@EJB
	private  ForumDAOLocal forumEjb;
	
	
	
	private Forum forum=new Forum();
	 private String val;
	
	public void doAddcomment()
	{
		forum.setComment(val);
		
		forumEjb.create(forum);
	}

	public Forum getForum() {
		return forum;
	}

	public void setForum(Forum forum) {
		this.forum = forum;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	
}

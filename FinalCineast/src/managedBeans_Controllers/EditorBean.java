package managedBeans_Controllers;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import edu.esprit.pfa.cineast.entities.Forum;
import edu.esprit.pfa.cineast.services.imp.ForumDAOLocal;

@ManagedBean(name="editorBean")
@ViewScoped
public class EditorBean {
	
	@EJB
	private ForumDAOLocal forumEjb;
	
	private List<Forum> forums;
	  
    private String value; 
    
   @PostConstruct
   public void init(){
	   setForums(forumEjb.retrieveAll());
   }
  
   //******* getters and setters ***********/
	public String getValue() {  
        return value;  
    }  
  
    public void setValue(String value) {  
        this.value = value;  
    }

	public List<Forum> getForums() {
		return forums;
	}

	public void setForums(List<Forum> forums) {
		this.forums = forums;
	}  
    
    
}  
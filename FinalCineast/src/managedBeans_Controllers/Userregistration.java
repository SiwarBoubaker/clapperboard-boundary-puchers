package managedBeans_Controllers;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.mail.MessagingException;

import edu.esprit.pfa.cineast.entities.RegistredUsers;
import edu.esprit.pfa.cineast.entities.Users;
import edu.esprit.pfa.cineast.services.imp.RegistredUsersDAOLocal;

@ManagedBean(name="userController")
@ViewScoped
public class Userregistration {
		
		@EJB
		RegistredUsersDAOLocal userEjb; 
		private RegistredUsers user = new RegistredUsers();
		
			
		public String doRegisterTeacher(){
			userEjb.create(user);
			try {
				AdminEmailSender.sendMail("cineast.admin@esprit.tn", "06930323", "Welcome to Cineast.com\nRegistration perform", "ahmed.chebaane@esprit.tn");
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "login?faces-redirect=true";
		}

		public RegistredUsers getUser() {
			return user;
		}

		public void setUser(RegistredUsers user) {
			this.user = user;
		}

	

}

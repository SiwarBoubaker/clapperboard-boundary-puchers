package managedBeans_Controllers;



import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

//import edu.esprit.pfa.cineast.entities.RegistredUsers;
//import edu.esprit.pfa.cineast.services.imp.RegistredUsersDAOLocal;
 
/**
 *
 * @author Ahmed
 */

@ManagedBean(name = "ContactController")
@RequestScoped
public class EmailSender {
	//@EJB
	//RegistredUsersDAOLocal registredUsersEjb;
	
	//private RegistredUsers registredUsers=new RegistredUsers();
	private String from=null;
	private String password=null;
	private String mesg=null;
	//private String to[];
	private String too=null;
	
    public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getMesg() {
		return mesg;
	}

	public void setMesg(String mesg) {
		this.mesg = mesg;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getToo() {
		return too;
	}

	public void setToo(String too) {
		this.too = too;
	}

	/*public RegistredUsers getRegistredUsers() {
		return registredUsers;
	}

	public void setRegistredUsers(RegistredUsers registredUsers) {
		this.registredUsers = registredUsers;
	}*/

	public  boolean sendMail() throws MessagingException
    {
		//from="ahmed_prep@live.fr";
		//to[0]="ahmed.chebaane@esprit.tn";
		//password="19911991";
		//mesg="c bonnnnnn";
		//to[0]=too;
        String host="smtp.gmail.com";
        Properties props =System.getProperties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", password);
        props.put(("mail.smtp.port"), 587);
        props.put(("mail.smtp.auth"), "true");
        
        Session session =Session.getDefaultInstance(props,null);
        MimeMessage mimeMesageg=new MimeMessage(session);
        try {
        /*	try{
        	
        	registredUsers= registredUsersEjb.retrieveByEmail(registredUsers.getEmail());
        	from=registredUsers.getEmail();
        	
        }catch(Exception ex){
            from=null;	
        }*/
            mimeMesageg.setFrom(new InternetAddress(from));
          
            InternetAddress toAddress=new InternetAddress(too);
            
              mimeMesageg.addRecipient(Message.RecipientType.TO, toAddress);
            // }
             mimeMesageg.setSubject("Confirmation of your message");
             mimeMesageg.setText(mesg);
             Transport transport=session.getTransport("smtp");
             transport.connect(host,from, password);
             transport.sendMessage(mimeMesageg, mimeMesageg.getAllRecipients());
             transport.close();
             return true;
        } catch (MessagingException ex) {
           ex.printStackTrace();
        }
        return false;
    }
    
}

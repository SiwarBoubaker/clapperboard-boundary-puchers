package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieFactsPK
 *
 */
@Embeddable
@Inheritance
public class MovieFactsPK implements Serializable {

	private int MovieId;
	private int idF;
	private static final long serialVersionUID = 1L;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + MovieId;
		result = prime * result + idF;
		return result;
	}

	public int getMovieId() {
		return MovieId;
	}

	public void setMovieId(int movieId) {
		MovieId = movieId;
	}

	public int getIdF() {
		return idF;
	}

	public void setIdF(int idF) {
		this.idF = idF;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieFactsPK other = (MovieFactsPK) obj;
		if (MovieId != other.MovieId)
			return false;
		if (idF != other.idF)
			return false;
		return true;
	}

	public MovieFactsPK() {
		super();
	}
   
}

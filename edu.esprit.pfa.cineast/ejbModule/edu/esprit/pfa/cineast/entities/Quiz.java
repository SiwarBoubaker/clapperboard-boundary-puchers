package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Quiz
 *
 */
@Entity

public class Quiz implements Serializable {
	
	private int id; 
	private String title; 
	private Movies movierelated; 
	private int numberquestion;
	@OneToMany(targetEntity=Question.class)
	private List<Question> questions; 
	
	public Quiz() {
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Movies getMovierelated() {
		return movierelated;
	}

	public void setMovierelated(Movies object) {
		this.movierelated = object;
	}

	
	public int getNumberquestion() {
		return numberquestion;
	}

	public void setNumberquestion(int numberquestion) {
		this.numberquestion = numberquestion;
	}

	private static final long serialVersionUID = 1L;

	
   
}

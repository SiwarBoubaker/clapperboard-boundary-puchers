package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: SoundTrack
 *
 */
@Entity

public class SoundTrack implements Serializable {

	
	private int Id;
	private String URL;
	private String description;
	private Movies movies;
	private static final long serialVersionUID = 1L;

	public SoundTrack() {
		super();
	}   
	@Id    
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getURL() {
		return this.URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@ManyToOne
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
   
}

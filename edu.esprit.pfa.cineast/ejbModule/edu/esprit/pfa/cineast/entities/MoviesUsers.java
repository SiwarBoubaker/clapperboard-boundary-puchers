package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MoviesUsers
 *
 */
@Entity

public class MoviesUsers implements Serializable {

	private MoviesUsersPK pk;
	private Movies movies;
	private Users users;
	private static final long serialVersionUID = 1L;

	public MoviesUsers() {
		super();
	}

	
	public MoviesUsers(MoviesUsersPK pk, Movies movies, Users users) {
		super();
		this.pk = pk;
		this.movies = movies;
		this.users = users;
	}


	@ManyToOne
	@JoinColumn(name = "MovieId", referencedColumnName = "MovieId", insertable = false, updatable = false)
	public Movies getMovies() {
		return movies;
	}

	public void setMovies(Movies movies) {
		this.movies = movies;
	}

	@ManyToOne
	@JoinColumn(name = "UsersId", referencedColumnName = "UsersId", insertable = false, updatable = false)
	public Users getUsers() {
		return users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}
    @EmbeddedId
	public MoviesUsersPK getPk() {
		return pk;
	}

	public void setPk(MoviesUsersPK pk) {
		this.pk = pk;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoviesUsers other = (MoviesUsers) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}
	
	
   
}

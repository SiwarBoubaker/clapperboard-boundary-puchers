package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieFacts
 *
 */
@Entity

public class MovieFacts implements Serializable {

	private Movies movies;
	private Facts facts;
	private MovieFactsPK movieFactsPK;
	
	@ManyToOne
	@JoinColumn(name="MovieId" , referencedColumnName="MovieId" ,insertable=false , updatable=false)
	public Movies getMovies() {
		return movies;
	}

	public void setMovies(Movies movies) {
		this.movies = movies;
	}

	@ManyToOne
	@JoinColumn(name="IdF" , referencedColumnName="IdF" ,insertable=false , updatable=false)public Facts getFacts() {
		return facts;
	}

	public void setFacts(Facts facts) {
		this.facts = facts;
	}
    @EmbeddedId
	public MovieFactsPK getMovieFactsPK() {
		return movieFactsPK;
	}

	public void setMovieFactsPK(MovieFactsPK movieFactsPK) {
		this.movieFactsPK = movieFactsPK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((movieFactsPK == null) ? 0 : movieFactsPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovieFacts other = (MovieFacts) obj;
		if (movieFactsPK == null) {
			if (other.movieFactsPK != null)
				return false;
		} else if (!movieFactsPK.equals(other.movieFactsPK))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;

	public MovieFacts() {
		super();
	}
   
}

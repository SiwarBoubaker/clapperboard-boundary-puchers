package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieTrailerVideo
 *
 */
@Embeddable

public class MovieTrailerVideo implements Serializable {

	private String URL;
	private String Vdescription;
	private static final long serialVersionUID = 1L;

	public MovieTrailerVideo() {
		super();
	}   
	
	public String getURL() {
		return this.URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}   

	public String getVdescription() {
		return Vdescription;
	}

	public void setVdescription(String vdescription) {
		Vdescription = vdescription;
	}
   
}

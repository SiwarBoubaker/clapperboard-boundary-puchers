package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RegistredUsersWishListPK
 *
 */
@Embeddable

public class RegistredUsersWishListPK implements Serializable {

	private int UsersId;
	private int Id;
	private static final long serialVersionUID = 1L;

	public RegistredUsersWishListPK() {
		super();
	}

	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + UsersId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistredUsersWishListPK other = (RegistredUsersWishListPK) obj;
		if (Id != other.Id)
			return false;
		if (UsersId != other.UsersId)
			return false;
		return true;
	}
	
	
   
}

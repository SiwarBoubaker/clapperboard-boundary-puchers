package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Facts
 *
 */
@Entity

public class Facts implements Serializable {

	
	private int idF;
	private String description;
	private String UrlPicture;
	private List<MovieFacts> movieFacts;
	private static final long serialVersionUID = 1L;

	public Facts() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getIdF() {
		return this.idF;
	}

	public void setIdF(int idF) {
		this.idF = idF;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getUrlPicture() {
		return this.UrlPicture;
	}

	public void setUrlPicture(String UrlPicture) {
		this.UrlPicture = UrlPicture;
	}
	
	@OneToMany(mappedBy="facts")
	public List<MovieFacts> getMovieFacts() {
		return movieFacts;
	}
	public void setMovieFacts(List<MovieFacts> movieFacts) {
		this.movieFacts = movieFacts;
	}
   
}

package edu.esprit.pfa.cineast.entities;


import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CinemaTheatre
 *
 */
@Entity

public class CinemaTheatre implements Serializable {

	
	private int Id;
	private String adress;
	private String description;
	private List<Movies> movie;
	private static final long serialVersionUID = 1L;

	public CinemaTheatre() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@OneToMany(mappedBy="cinemaT")
	public List<Movies> getMovie() {
		return movie;
	}
	public void setMovie(List<Movies> movie) {
		this.movie = movie;
	}
	   
}

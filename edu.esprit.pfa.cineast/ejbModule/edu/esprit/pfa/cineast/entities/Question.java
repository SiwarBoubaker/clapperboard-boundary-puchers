package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: Question
 *
 */
@Entity

public class Question implements Serializable {

	
	private int IdQ;
	private String libelle;
	@ManyToOne(targetEntity=Quiz.class)
	private Quiz quiz; 
	@OneToOne(targetEntity=Reponse.class)
	private Reponse reponse; 
	

	private static final long serialVersionUID = 1L;

	public Question() {
		super();
	}   
	
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
    @Id
	public int getIdQ() {
		return IdQ;
	}

	public void setIdQ(int idQ) {
		IdQ = idQ;
	}


}

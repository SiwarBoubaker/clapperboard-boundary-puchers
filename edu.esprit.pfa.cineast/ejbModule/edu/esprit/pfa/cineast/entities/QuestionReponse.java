package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: QuestionReponse
 *
 */
@Entity

public class QuestionReponse implements Serializable {

	private QuestionReponsePK questionReponsePK;
	private Question question;
	private Reponse reponse;
	private static final long serialVersionUID = 1L;

	public QuestionReponse() {
		super();
	}

	@EmbeddedId
	public QuestionReponsePK getQuestionReponsePK() {
		return questionReponsePK;
	}

	public void setQuestionReponsePK(QuestionReponsePK questionReponsePK) {
		this.questionReponsePK = questionReponsePK;
	}

	
	@ManyToOne
	@JoinColumn(name = "IdQ", referencedColumnName = "IdQ", insertable = false, updatable = false)
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	
	@ManyToOne
	@JoinColumn(name = "Id", referencedColumnName = "Id", insertable = false, updatable = false)
	public Reponse getReponse() {
		return reponse;
	}

	public void setReponse(Reponse reponse) {
		this.reponse = reponse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((questionReponsePK == null) ? 0 : questionReponsePK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionReponse other = (QuestionReponse) obj;
		if (questionReponsePK == null) {
			if (other.questionReponsePK != null)
				return false;
		} else if (!questionReponsePK.equals(other.questionReponsePK))
			return false;
		return true;
	}
	
	
   
}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: RegistredUsers
 *
 */
@Entity

public class RegistredUsers extends Users implements Serializable {
 
	private List<RegistredUsersWishList> registredUsersWishLists;
	
	
	private static final long serialVersionUID = 1L;

	public RegistredUsers() {
		super();
	}

	@OneToMany(mappedBy="registredUsers")
	public List<RegistredUsersWishList> getRegistredUsersWishLists() {
		return registredUsersWishLists;
	}

	public void setRegistredUsersWishLists(List<RegistredUsersWishList> registredUsersWishLists) {
		this.registredUsersWishLists = registredUsersWishLists;
	}
   
}

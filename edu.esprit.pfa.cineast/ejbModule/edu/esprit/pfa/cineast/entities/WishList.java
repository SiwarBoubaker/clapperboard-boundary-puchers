package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: WishList
 *
 */
@Entity

public class WishList implements Serializable {

	
	private int Id;
	private String title;
	private List<RegistredUsersWishList> registredUsersWishLists;
	private static final long serialVersionUID = 1L;

	public WishList() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@OneToMany(mappedBy="wishList")
	public List<RegistredUsersWishList> getRegistredUsersWishLists() {
		return registredUsersWishLists;
	}
	public void setRegistredUsersWishLists(List<RegistredUsersWishList> registredUsersWishLists) {
		this.registredUsersWishLists = registredUsersWishLists;
	}
   
}

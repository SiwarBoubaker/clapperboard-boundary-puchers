package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RegistredUsersWishList
 *
 */
@Entity

public class RegistredUsersWishList implements Serializable {

	private RegistredUsersWishListPK registredUsersWishListPK;
	private RegistredUsers registredUsers;
	private WishList wishList;
	private static final long serialVersionUID = 1L;

	public RegistredUsersWishList() {
		super();
	}

	@EmbeddedId
	public RegistredUsersWishListPK getRegistredUsersWishListPK() {
		return registredUsersWishListPK;
	}

	public void setRegistredUsersWishListPK(RegistredUsersWishListPK registredUsersWishListPK) {
		this.registredUsersWishListPK = registredUsersWishListPK;
	}

	@ManyToOne
	@JoinColumn(name = "UsersId", referencedColumnName ="UsersId", insertable = false, updatable = false)
	public RegistredUsers getRegistredUsers() {
		return registredUsers;
	}

	public void setRegistredUsers(RegistredUsers registredUsers) {
		this.registredUsers = registredUsers;
	}
	
	@ManyToOne
	@JoinColumn(name = "Id", referencedColumnName ="Id", insertable = false, updatable = false)
	public WishList getWishList() {
		return wishList;
	}

	public void setWishList(WishList wishList) {
		this.wishList = wishList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((registredUsersWishListPK == null) ? 0
						: registredUsersWishListPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistredUsersWishList other = (RegistredUsersWishList) obj;
		if (registredUsersWishListPK == null) {
			if (other.registredUsersWishListPK != null)
				return false;
		} else if (!registredUsersWishListPK
				.equals(other.registredUsersWishListPK))
			return false;
		return true;
	}
	
	
   
}

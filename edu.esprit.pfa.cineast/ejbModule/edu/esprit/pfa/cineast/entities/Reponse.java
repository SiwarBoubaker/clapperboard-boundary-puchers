package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Reponse
 *
 */
@Entity

public class Reponse implements Serializable {

	
	private int Id;
	private String libelle;
	@OneToOne(targetEntity=Question.class)
	private Question qesution; 
	
	private static final long serialVersionUID = 1L;

	public Reponse() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

   
}

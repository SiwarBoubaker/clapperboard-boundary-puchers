package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.String;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Movies
 *
 */
@Entity

public class Movies implements Serializable {

	
	private int MovieId;
	private String title;
	private Calendar year_released;
	private String description;
	private String genre;
	private String photo;
	private Boolean state_premiere;
	private boolean boxOffice;
	private CinemaTheatre cinemaT;
	
	private float Benifits; 
	
	@OneToMany(mappedBy="movies")
	public List<MovieFacts> getMovieFacts() {
		return movieFacts;
	}

	public void setMovieFacts(List<MovieFacts> movieFacts) {
		this.movieFacts = movieFacts;
	}

	private List<MoviesUsers> movieUser;
	private List<SoundTrack> soundTracks;
	private MovieTrailerVideo movieTrailerVideo;
	private List<MoviesParticipate> movieParticipate;
	private List<MovieFacts> movieFacts;
	private static final long serialVersionUID = 1L;

	public Movies() {
		super();
	}   
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	 
	public int getMovieId() {
		return MovieId;
	}
	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public Calendar getYear_released() {
		return this.year_released;
	}

	public void setYear_released(Calendar year_released) {
		this.year_released = year_released;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}   
	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}   
	public Boolean getState_premiere() {
		return this.state_premiere;
	}

	public void setState_premiere(Boolean state_premiere) {
		this.state_premiere = state_premiere;
	}


	@ManyToOne
	public CinemaTheatre getCinemaT() {
		return cinemaT;
	}
	public void setCinemaT(CinemaTheatre cinemaT) {
		this.cinemaT = cinemaT;
	}
	@OneToMany(mappedBy="movies")
	public List<MoviesUsers> getMovieUser() {
		return movieUser;
	}
	public void setMovieUser(List<MoviesUsers> movieUser) {
		this.movieUser = movieUser;
	}
	@OneToMany(mappedBy="movies")
	public List<MoviesParticipate> getMovieParticipate() {
		return movieParticipate;
	}
	public void setMovieParticipate(List<MoviesParticipate> movieParticipate) {
		this.movieParticipate = movieParticipate;
	}

	@OneToMany(mappedBy="movies")
	public List<SoundTrack> getSoundTracks() {
		return soundTracks;
	}

	public void setSoundTracks(List<SoundTrack> soundTracks) {
		this.soundTracks = soundTracks;
	}
  @Embedded
	public MovieTrailerVideo getMovieTrailerVideo() {
		return movieTrailerVideo;
	}

	public void setMovieTrailerVideo(MovieTrailerVideo movieTrailerVideo) {
		this.movieTrailerVideo = movieTrailerVideo;
	}

	public float getBenifits() {
		return Benifits;
	}

	public void setBenifits(float benifits) {
		Benifits = benifits;
	}

	public boolean isBoxOffice() {
		return boxOffice;
	}

	public void setBoxOffice(boolean boxOffice) {
		this.boxOffice = boxOffice;
	}


	
   
}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MoviesParticipate
 *
 */
@Entity

public class MoviesParticipate implements Serializable {

	private MoviesParticipatePK moviepPK;
	private Movies movies;
	private Participate participate;
	private static final long serialVersionUID = 1L;

	public MoviesParticipate() {
		super();
	}

	@EmbeddedId
	public MoviesParticipatePK getMoviepPK() {
		return moviepPK;
	}

	public void setMoviepPK(MoviesParticipatePK moviepPK) {
		this.moviepPK = moviepPK;
	}

	@ManyToOne
	@JoinColumn(name = "MovieId", referencedColumnName = "MovieId", insertable = false, updatable = false)
	public Movies getMovies() {
		return movies;
	}

	public void setMovies(Movies movies) {
		this.movies = movies;
	}

	@ManyToOne
	@JoinColumn(name = "Idparticipate", referencedColumnName = "Idparticipate", insertable = false, updatable = false)
	public Participate getParticipate() {
		return participate;
	}

	public void setParticipate(Participate participate) {
		this.participate = participate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((moviepPK == null) ? 0 : moviepPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoviesParticipate other = (MoviesParticipate) obj;
		if (moviepPK == null) {
			if (other.moviepPK != null)
				return false;
		} else if (!moviepPK.equals(other.moviepPK))
			return false;
		return true;
	}
	
	
	
   
}

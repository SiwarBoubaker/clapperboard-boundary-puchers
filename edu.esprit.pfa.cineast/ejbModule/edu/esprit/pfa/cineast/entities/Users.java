package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Users
 * 
 */
@Entity
public abstract class Users implements Serializable {

	private int UsersId;
	private int CIN;
	private String name;
	private String surname;
	private String Email;
	private String nationality;
	private Date Birthday;
	private String sexe;
	private String password;
	private Roles role;
	private List<MoviesUsers> movieUser;
	
	private static final long serialVersionUID = 1L;

	public Users() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}

	public int getCIN() {
		return this.CIN;
	}

	public void setCIN(int CIN) {
		this.CIN = CIN;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return this.Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}

	@OneToMany(mappedBy = "users")
	public List<MoviesUsers> getMovieUser() {
		return movieUser;
	}

	public void setMovieUser(List<MoviesUsers> movieUser) {
		this.movieUser = movieUser;
	}

	@ManyToOne
	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}


}

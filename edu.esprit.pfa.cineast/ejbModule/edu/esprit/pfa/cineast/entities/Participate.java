package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Participate
 *
 */
@Entity

public abstract class Participate implements Serializable {

	
	private int Idparticipate;
	private String name;
	private String surname;
	private String nationality;
	private Date Birthday;
	private String sexe;
	private int CIN;
	private String libelle;
	private String Roles_Participate;
	private List<MoviesParticipate> movieParticipate;
	private static final long serialVersionUID = 1L;

	public Participate() {
		super();
	} 
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)

	public int getIdparticipate() {
		return Idparticipate;
	}
	public void setIdparticipate(int idparticipate) {
		Idparticipate = idparticipate;
	}
	
	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getRoles_Participate() {
		return Roles_Participate;
	}
	public void setRoles_Participate(String roles_Participate) {
		Roles_Participate = roles_Participate;
	}
	@OneToMany(mappedBy="participate")
	public List<MoviesParticipate> getMovieParticipate() {
		return movieParticipate;
	}
	public void setMovieParticipate(List<MoviesParticipate> movieParticipate) {
		this.movieParticipate = movieParticipate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public int getCIN() {
		return CIN;
	}

	public void setCIN(int cIN) {
		CIN = cIN;
	}
   
}

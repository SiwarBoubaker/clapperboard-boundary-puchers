package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: QuestionReponsePK
 *
 */
@Embeddable

public class QuestionReponsePK implements Serializable {

	private int IdQ;
	private int Id;
	private static final long serialVersionUID = 1L;

	public QuestionReponsePK() {
		super();
	}

	public QuestionReponsePK(int idQ, int id) {
		super();
		IdQ = idQ;
		Id = id;
	}

	public int getIdQ() {
		return IdQ;
	}

	public void setIdQ(int idQ) {
		IdQ = idQ;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + IdQ;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionReponsePK other = (QuestionReponsePK) obj;
		if (Id != other.Id)
			return false;
		if (IdQ != other.IdQ)
			return false;
		return true;
	}
	
	
   
}

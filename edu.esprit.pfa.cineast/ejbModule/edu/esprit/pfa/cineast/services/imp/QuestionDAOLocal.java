package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.Question;

@Local
public interface QuestionDAOLocal {
	
	public void create(Question b);

	public Question	 retrieve(int id);
	
	public void update(Question b);
	
	public void delete(Question b);
	
	public List<Question> retrieveAll();

}

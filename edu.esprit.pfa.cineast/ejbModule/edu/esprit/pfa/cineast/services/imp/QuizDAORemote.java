package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Quiz;

@Remote
public interface QuizDAORemote {
	
	public void create(Quiz b);

	public Quiz	 retrieve(int id);
	
	public void update(Quiz b);
	
	public void delete(Quiz b);
	
	public List<Quiz> retrieveAll();

}

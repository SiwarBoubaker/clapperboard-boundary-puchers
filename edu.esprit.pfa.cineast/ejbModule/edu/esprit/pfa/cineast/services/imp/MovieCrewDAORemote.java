package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.MovieCrew;


@Remote
public interface MovieCrewDAORemote {

    public void create(MovieCrew b);
	
	public MovieCrew retrieve(int id);
	
	public void update(MovieCrew b);
	
	public void delete(MovieCrew b);
	
	public List<MovieCrew> retrieveAll();

}

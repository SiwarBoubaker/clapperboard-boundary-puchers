package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Question;

/**
 * Session Bean implementation class QuestionDAO
 */
@Stateless
@LocalBean
public class QuestionDAO implements QuestionDAORemote, QuestionDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public QuestionDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Question b) {
		em.persist(b);
		
	}

	@Override
	public Question retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Question.class, id);
	}

	@Override
	public void update(Question b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Question b) {
		em.remove(b);
		
	}

	@Override
	public List<Question> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

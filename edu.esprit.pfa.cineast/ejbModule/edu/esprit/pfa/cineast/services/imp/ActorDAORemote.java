package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Actor;


@Remote
public interface ActorDAORemote {

	public void create(Actor b);
	
	public Actor retrieve(int id);
	
	public void update(Actor b);
	
	public void delete(Actor b);
	
	public List<Actor> retrieveAll();

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Movies;

@Remote
public interface MoviesDAORemote {
	
	public void create(Movies b);
	
	public Movies retrieve(int id);
	
	public void update(Movies b);
	
	public void delete(Movies b);
	
	public List<Movies> retrieveAll();

}

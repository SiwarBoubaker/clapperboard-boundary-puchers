package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;


import edu.esprit.pfa.cineast.entities.Facts;

@Remote
public interface FactsDAORemote {
	
	  public void create(Facts f);
		
		public Facts retrieve(int idF);
		
		public void update(Facts f);
		
		public void delete(Facts f);
		
		public List<Facts> retrieveAll();

}

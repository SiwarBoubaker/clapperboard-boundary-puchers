package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Cineast;

/**
 * Session Bean implementation class CineastDAO
 */
@Stateless
@LocalBean
public class CineastDAO implements CineastDAORemote, CineastDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public CineastDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Cineast b) {
		em.persist(b);
		
	}

	@Override
	public Cineast retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Cineast.class, id);
	}

	@Override
	public void update(Cineast b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Cineast b) {
		em.remove(b);
		
	}

	@Override
	public List<Cineast> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Question;

@Remote
public interface QuestionDAORemote {
	
	public void create(Question b);

	public Question	 retrieve(int id);
	
	public void update(Question b);
	
	public void delete(Question b);
	
	public List<Question> retrieveAll();

}

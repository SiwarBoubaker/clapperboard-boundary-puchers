package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Admin;

/**
 * Session Bean implementation class AdminDAO
 */
@Stateless
@LocalBean
public class AdminDAO implements AdminDAORemote, AdminDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public AdminDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Admin b) {
		em.persist(b);
		
	}

	@Override
	public Admin retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Admin.class, id);
	}

	@Override
	public void update(Admin b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Admin b) {
		em.remove(b);
		
	}

	@Override
	public List<Admin> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Reponse;

/**
 * Session Bean implementation class ReponseDAO
 */
@Stateless
@LocalBean
public class ReponseDAO implements ReponseDAORemote, ReponseDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public ReponseDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Reponse b) {
		em.persist(b);
		
	}

	@Override
	public Reponse retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Reponse.class, id);
	}

	@Override
	public void update(Reponse b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Reponse b) {
		em.remove(b);
		
	}

	@Override
	public List<Reponse> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

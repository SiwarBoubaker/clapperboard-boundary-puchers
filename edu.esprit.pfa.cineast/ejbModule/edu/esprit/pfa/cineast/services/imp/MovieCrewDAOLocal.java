package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.MovieCrew;

@Local
public interface MovieCrewDAOLocal {
	
    public void create(MovieCrew b);
	
	public MovieCrew retrieve(int id);
	
	public void update(MovieCrew b);
	
	public void delete(MovieCrew b);
	
	public List<MovieCrew> retrieveAll();

}

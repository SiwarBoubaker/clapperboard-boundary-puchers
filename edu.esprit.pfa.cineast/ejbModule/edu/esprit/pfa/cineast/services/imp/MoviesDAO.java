package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Movies;

/**
 * Session Bean implementation class MoviesDAO
 */
@Stateless
@LocalBean
public class MoviesDAO implements MoviesDAORemote, MoviesDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public MoviesDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Movies b) {
		em.persist(b);
		
	}

	@Override
	public Movies retrieve(int id) {
		return em.find(Movies.class, id);
	}

	@Override
	public void update(Movies b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Movies b) {
		em.remove(b);
		
	}

	@Override
	public List<Movies> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Quiz;

/**
 * Session Bean implementation class QuizDAO
 */
@Stateless
public class QuizDAO implements QuizDAORemote, QuizDAOLocal {

    /**
     * Default constructor. 
     */
	@PersistenceContext
	EntityManager em; 
	
    public QuizDAO() {
       
    }

	@Override
	public void create(Quiz b) {
		em.persist(b); 
		
	}

	@Override
	public Quiz retrieve(int id) {
		
		return em.find(Quiz.class, id);}
	

	@Override
	public void update(Quiz b) {
		em.merge(b); 
	}

	@Override
	public void delete(Quiz b) {
	em.remove(b); 
		
	}

	@Override
	public List<Quiz> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

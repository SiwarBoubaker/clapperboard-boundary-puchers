package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Facts;

/**
 * Session Bean implementation class FactsDAO
 */
@Stateless
@LocalBean
public class FactsDAO implements FactsDAORemote, FactsDAOLocal {

    @PersistenceContext
    EntityManager em;
    
    public FactsDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Facts f) {
		
		em.persist(f);
		
	}

	@Override
	public Facts retrieve(int idF) {
		
		return em.find(Facts.class, idF);
	}

	@Override
	public void update(Facts f) {
		em.merge(f);
		
	}

	@Override
	public void delete(Facts f) {
		em.remove(f);
		
	}

	@Override
	public List<Facts> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

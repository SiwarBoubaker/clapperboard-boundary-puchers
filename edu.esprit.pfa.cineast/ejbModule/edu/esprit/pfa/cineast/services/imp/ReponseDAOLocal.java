package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.Reponse;

@Local
public interface ReponseDAOLocal {
	
	public void create(Reponse b);

	public Reponse retrieve(int id);
	
	public void update(Reponse b);
	
	public void delete(Reponse b);
	
	public List<Reponse> retrieveAll();

}

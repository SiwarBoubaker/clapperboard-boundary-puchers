package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.Quiz;


@Local
public interface QuizDAOLocal {
	
	public void create(Quiz b);

	public Quiz	 retrieve(int id);
	
	public void update(Quiz b);
	
	public void delete(Quiz b);
	
	public List<Quiz> retrieveAll();

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;


import edu.esprit.pfa.cineast.entities.Cineast;

@Remote
public interface CineastDAORemote {
	
	    public void create(Cineast b);
		
		public Cineast retrieve(int id);
		
		public void update(Cineast b);
		
		public void delete(Cineast b);

		public List<Cineast> retrieveAll();
}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.CinemaTheatre;

/**
 * Session Bean implementation class CinemaTheatreDAO
 */
@Stateless
@LocalBean
public class CinemaTheatreDAO implements CinemaTheatreDAORemote, CinemaTheatreDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public CinemaTheatreDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(CinemaTheatre b) {
		em.persist(b);
		
	}

	@Override
	public CinemaTheatre retrieve(int id) {
		return em.find(CinemaTheatre.class, id);
	}

	@Override
	public void update(CinemaTheatre b) {
		em.merge(b);
		
	}

	@Override
	public void delete(CinemaTheatre b) {
		em.remove(b);
		
	}

	@Override
	public List<CinemaTheatre> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

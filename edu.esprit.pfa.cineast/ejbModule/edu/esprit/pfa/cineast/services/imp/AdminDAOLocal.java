package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.Admin;

@Local
public interface AdminDAOLocal {
	
    public void create(Admin b);
	
	public Admin retrieve(int id);
	
	public void update(Admin b);
	
	public void delete(Admin b);
	
	public List<Admin> retrieveAll();


}

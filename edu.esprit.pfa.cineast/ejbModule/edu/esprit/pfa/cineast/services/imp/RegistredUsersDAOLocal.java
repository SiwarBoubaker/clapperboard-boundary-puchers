package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.RegistredUsers;

@Local
public interface RegistredUsersDAOLocal {
	
	    public void create(RegistredUsers b);
		
		public RegistredUsers retrieve(int id);
		
		public void update(RegistredUsers b);
		
		public void delete(RegistredUsers b);
		
		public List<RegistredUsers> retrieveAll();

}

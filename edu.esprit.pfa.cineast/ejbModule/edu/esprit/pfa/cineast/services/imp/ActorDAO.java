package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.Actor;


/**
 * Session Bean implementation class ActorDAO
 */
@Stateless
@LocalBean
public class ActorDAO implements ActorDAORemote, ActorDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public ActorDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Actor b) {
		em.persist(b);
		
	}

	@Override
	public Actor retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Actor.class, id);
	}

	@Override
	public void update(Actor b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Actor b) {
		em.remove(b);
		
	}

	@Override
	public List<Actor> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

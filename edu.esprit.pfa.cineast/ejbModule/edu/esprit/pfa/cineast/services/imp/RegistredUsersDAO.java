package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.RegistredUsers;

/**
 * Session Bean implementation class RegistredUsersDAO
 */
@Stateless
@LocalBean
public class RegistredUsersDAO implements RegistredUsersDAORemote, RegistredUsersDAOLocal {

    @PersistenceContext
    EntityManager em;
    
    public RegistredUsersDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(RegistredUsers b) {
		em.persist(b);
		
	}

	@Override
	public RegistredUsers retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(RegistredUsers.class, id);
	}

	@Override
	public void update(RegistredUsers b) {
		em.merge(b);
		
	}

	@Override
	public void delete(RegistredUsers b) {
		em.remove(b);
		
	}

	@Override
	public List<RegistredUsers> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

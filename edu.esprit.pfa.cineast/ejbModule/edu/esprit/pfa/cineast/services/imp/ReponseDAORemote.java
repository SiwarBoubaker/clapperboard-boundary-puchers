package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Reponse;

@Remote
public interface ReponseDAORemote {
	
	public void create(Reponse b);

	public Reponse	 retrieve(int id);
	
	public void update(Reponse b);
	
	public void delete(Reponse b);
	
	public List<Reponse> retrieveAll();

}

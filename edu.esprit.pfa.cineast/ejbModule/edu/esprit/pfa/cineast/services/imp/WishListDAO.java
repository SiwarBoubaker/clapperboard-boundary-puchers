package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.WishList;

/**
 * Session Bean implementation class WishListDAO
 */
@Stateless
@LocalBean
public class WishListDAO implements WishListDAORemote, WishListDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public WishListDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(WishList b) {
		em.persist(b);
		
	}

	@Override
	public WishList retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(WishList.class, id);
	}

	@Override
	public void update(WishList b) {
		em.merge(b);
		
	}

	@Override
	public void delete(WishList b) {
		em.remove(b);
		
	}

	@Override
	public List<WishList> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.esprit.pfa.cineast.entities.MovieCrew;

/**
 * Session Bean implementation class MovieCrewDAO
 */
@Stateless
@LocalBean
public class MovieCrewDAO implements MovieCrewDAORemote, MovieCrewDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public MovieCrewDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(MovieCrew b) {
		em.persist(b);
		
	}

	@Override
	public MovieCrew retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(MovieCrew.class, id);
	}

	@Override
	public void update(MovieCrew b) {
		em.merge(b);
		
	}

	@Override
	public void delete(MovieCrew b) {
		em.remove(b);
		
	}

	@Override
	public List<MovieCrew> retrieveAll() {
		// TODO Auto-generated method stub
		return null;
	}

}

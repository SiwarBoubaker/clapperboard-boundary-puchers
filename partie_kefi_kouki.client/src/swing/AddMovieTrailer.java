package swing;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.SwingConstants;

import services.interfaces.TrailerDAORemote;
import util.ServiceLocator;
import entities.Trailer;

public class AddMovieTrailer extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	
	private static TrailerDAORemote proxyTrailer = (TrailerDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/TrailerDAO!services.interfaces.TrailerDAORemote");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddMovieTrailer frame = new AddMovieTrailer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddMovieTrailer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 600);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewActor = new JLabel("NEW");
		lblNewActor.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewActor.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblNewActor.setBounds(46, 48, 251, 41);
		getContentPane().add(lblNewActor);
		
		JLabel lblNewLabel = new JLabel("Movie");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBounds(57, 205, 84, 29);
		getContentPane().add(lblNewLabel);
		
		JLabel lblGender = new JLabel("Vid Quality");
		lblGender.setFont(new Font("Arial", Font.BOLD, 15));
		lblGender.setBounds(57, 322, 84, 29);
		getContentPane().add(lblGender);
		
		JLabel lblLastName = new JLabel("Duration");
		lblLastName.setFont(new Font("Arial", Font.BOLD, 15));
		lblLastName.setBounds(57, 260, 84, 29);
		getContentPane().add(lblLastName);
		
		JButton btnNewButton = new JButton("SAVE");
		btnNewButton.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButton.setBounds(187, 510, 138, 51);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButtonBack = new JButton("BACK");
		btnNewButtonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MovieTrailer movietrailer = new MovieTrailer();
				setVisible(false);
				movietrailer.setVisible(true);
				
				
				
			}
		});
		btnNewButtonBack.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButtonBack.setBounds(10, 510, 138, 51);
		getContentPane().add(btnNewButtonBack);
		
		textField = new JTextField();
		textField.setBounds(185, 206, 106, 29);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(185, 265, 106, 29);
		getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(185, 327, 106, 29);
		getContentPane().add(textField_2);
		
		JLabel lblCrewMember = new JLabel("MOVIE TRAILER");
		lblCrewMember.setHorizontalAlignment(SwingConstants.CENTER);
		lblCrewMember.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblCrewMember.setBounds(10, 100, 334, 41);
		contentPane.add(lblCrewMember);
	}

}

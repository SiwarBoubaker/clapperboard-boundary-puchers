package swing;

import java.util.List;

import util.ServiceLocator;

import entities.Actor;
import services.interfaces.ActorDAORemote;

import entities.CrewMember;
import services.interfaces.CrewMemberDAORemote;



public class ServiceDelegate {

	private static CrewMemberDAORemote proxyCrewMember = (CrewMemberDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/CrewMemberDAO!services.interfaces.CrewMemberDAORemote");

	private static ActorDAORemote proxyActor = (ActorDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/ActorDAO!services.interfaces.ActorDAORemote");

	/*public static void addCrewMember(CrewMember CrewMember) {

		proxyCrewMember.create(CrewMember);
	}

	public static void addActor(Actor Actor) {

		proxyActor.create(Actor);
	}

	public static List<Actor> findAllActor() {
		if (proxyActor.retrieveAll().isEmpty()) {
		}
		return proxyActor.retrieveAll();

	}*/
	public static void main(String[] args)
	{
	Actor actor= new Actor();
	actor.setFirstName("bruce");
	actor.setLastName("willis");
	proxyActor.create(actor);
	}

}

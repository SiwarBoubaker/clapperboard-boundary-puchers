package swing;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import services.interfaces.ActorDAORemote;
import util.ServiceLocator;
import entities.Actor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ActorSwing extends JFrame {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table = new JTable();
	private JTextField textField;
	
	private static ActorDAORemote proxyActor = (ActorDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/ActorDAO!services.interfaces.ActorDAORemote");
	
	private List <Actor> actors;

	/**
	 * 	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ActorSwing frame = new ActorSwing();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	

	
	
	
	
	public ActorSwing() {
		
		actors = new ArrayList <Actor>();
		
		actors=proxyActor.retrieveAll();
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 550);
		this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(66, 120, 487, 260);
		contentPane.add(scrollPane);
		
		JButton btnAddActor = new JButton("Add Actor");
		btnAddActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AddActor addactor = new AddActor();
				setVisible(false);
				addactor.setVisible(true);
				
				
				
			}
		});
		btnAddActor.setBounds(43, 444, 109, 43);
		getContentPane().add(btnAddActor);

		JButton btnUpdateActor = new JButton("Update Actor");
		btnUpdateActor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				actors = new ArrayList <Actor>();
				actors=proxyActor.retrieveAll();
				
				
				
			//	for (int i=0; i < actors.size() ; i++)

			
				
				
				
				
				
				
			}
		});
		btnUpdateActor.setBounds(258, 444, 109, 43);
		getContentPane().add(btnUpdateActor);
		
		JButton btnDeleteActor = new JButton("Delete Actor");
		btnDeleteActor.setBounds(472, 444, 109, 43);
		getContentPane().add(btnDeleteActor);

		
		
		
		Object[][] data= new Object [actors.size()]	[7];	
		
		int i =0;
		for ( Actor a : actors){
			data[i][0]=a.getParticipantId();
			data[i][1]=a.getFirstName();
			data[i][2]=a.getLastName();
			data[i][3]=a.getGender();
			data[i][4]=a.getNationality();
			data[i][5]=a.getBirthDate();
			data[i][6]=a.getAwards();
			i++; }

		
		

		
		


		table.setModel(new DefaultTableModel(data ,new String[] {
				"Id", "First_name", "Last_name", "Gender", "Nationality", "Birthdate", "Award"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		}

				);
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("B A C K");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MainInterface maininterface = new MainInterface();
				setVisible(false);
				maininterface.setVisible(true);
				
				
			}
		});
		btnNewButton_1.setBounds(29, 23, 93, 55);
		getContentPane().add(btnNewButton_1);
		
		JLabel lblSearch = new JLabel("Search");
		lblSearch.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSearch.setHorizontalAlignment(SwingConstants.CENTER);
		lblSearch.setBounds(381, 84, 69, 25);
		contentPane.add(lblSearch);
		
		textField = new JTextField();
		textField.setBounds(460, 84, 93, 25);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblActors = new JLabel("ACTORS");
		lblActors.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblActors.setBounds(229, 11, 176, 41);
		contentPane.add(lblActors);
		
	}


}



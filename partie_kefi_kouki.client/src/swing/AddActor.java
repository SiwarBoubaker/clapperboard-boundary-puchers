package swing;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.util.List;

import util.ServiceLocator;
import entities.Actor;
import services.interfaces.ActorDAORemote;

public class AddActor extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField_FirstName;
	private JTextField textField_LastName;
	private JTextField textField_Gender;
	private JTextField textField_Nationality;
	private JTextField textField_Birthdate;
	private JTextField textField_Award;
	
	private static ActorDAORemote proxyActor = (ActorDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/ActorDAO!services.interfaces.ActorDAORemote");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddActor frame = new AddActor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddActor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 600);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewActor = new JLabel("NEW ACTOR");
		lblNewActor.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblNewActor.setBounds(41, 21, 251, 41);
		getContentPane().add(lblNewActor);
		
		JLabel lblNewLabel = new JLabel("First Name");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBounds(59, 103, 84, 29);
		getContentPane().add(lblNewLabel);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("Arial", Font.BOLD, 15));
		lblGender.setBounds(59, 220, 84, 29);
		getContentPane().add(lblGender);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("Arial", Font.BOLD, 15));
		lblLastName.setBounds(59, 158, 84, 29);
		getContentPane().add(lblLastName);
		
		JLabel lblNationality = new JLabel("Nationality");
		lblNationality.setFont(new Font("Arial", Font.BOLD, 15));
		lblNationality.setBounds(59, 280, 84, 29);
		getContentPane().add(lblNationality);
		
		JLabel lblBirthdate = new JLabel("Birthdate");
		lblBirthdate.setFont(new Font("Arial", Font.BOLD, 15));
		lblBirthdate.setBounds(59, 339, 84, 29);
		getContentPane().add(lblBirthdate);
		
		JLabel lblAward = new JLabel("Award");
		lblAward.setFont(new Font("Arial", Font.BOLD, 15));
		lblAward.setBounds(59, 402, 84, 29);
		getContentPane().add(lblAward);
		
		JButton btnNewButton = new JButton("SAVE");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
		Actor actor= new Actor();
		actor.setFirstName(textField_FirstName.getText());
		actor.setLastName(textField_LastName.getText());
		actor.setGender(textField_Gender.getText());
		actor.setNationality(textField_Nationality.getText());
		actor.setBirthDate(textField_Birthdate.getText());
		actor.setAwards(textField_Award.getText());
		proxyActor.create(actor);
				
				
				
				
				
				
		ActorSwing actor1 = new ActorSwing();
		setVisible(false);
		actor1.setVisible(true);
				
				
				
				
				
				
				
				
				
				
			}
		});
		btnNewButton.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButton.setBounds(187, 510, 138, 51);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButtonBack = new JButton("BACK");
		btnNewButtonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ActorSwing actor = new ActorSwing();
				setVisible(false);
				actor.setVisible(true);
				
				
				
			}
		});
		btnNewButtonBack.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButtonBack.setBounds(10, 510, 138, 51);
		getContentPane().add(btnNewButtonBack);
		
		textField_FirstName = new JTextField();
		textField_FirstName.setBounds(187, 104, 106, 29);
		getContentPane().add(textField_FirstName);
		textField_FirstName.setColumns(10);
		
		textField_LastName = new JTextField();
		textField_LastName.setColumns(10);
		textField_LastName.setBounds(187, 163, 106, 29);
		getContentPane().add(textField_LastName);
		
		textField_Gender = new JTextField();
		textField_Gender.setColumns(10);
		textField_Gender.setBounds(187, 225, 106, 29);
		getContentPane().add(textField_Gender);
		
		textField_Nationality = new JTextField();
		textField_Nationality.setColumns(10);
		textField_Nationality.setBounds(187, 285, 106, 29);
		getContentPane().add(textField_Nationality);
		
		textField_Birthdate = new JTextField();
		textField_Birthdate.setColumns(10);
		textField_Birthdate.setBounds(187, 344, 106, 29);
		getContentPane().add(textField_Birthdate);
		
		textField_Award = new JTextField();
		textField_Award.setColumns(10);
		textField_Award.setBounds(187, 403, 106, 29);
		getContentPane().add(textField_Award);
	}

}

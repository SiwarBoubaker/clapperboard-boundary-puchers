package swing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import services.interfaces.TrailerDAORemote;
import util.ServiceLocator;
import entities.Trailer;

public class MovieTrailer extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	
	private static TrailerDAORemote proxyTrailer = (TrailerDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/TrailerDAO!services.interfaces.TrailerDAORemote");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MovieTrailer frame = new MovieTrailer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MovieTrailer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 550);
		this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(66, 120, 487, 260);
		contentPane.add(scrollPane);
		
		JButton btnAddMovieTrailer = new JButton("Add MovieTrailer");
		btnAddMovieTrailer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AddMovieTrailer addmovietrailer = new AddMovieTrailer();
				setVisible(false);
				addmovietrailer.setVisible(true);
				
				
				
			}
		});
		btnAddMovieTrailer.setBounds(46, 444, 154, 43);
		getContentPane().add(btnAddMovieTrailer);

		JButton btnUpdateMovieTrailer = new JButton("Update MovieTrailer");
		btnUpdateMovieTrailer.setBounds(241, 444, 154, 43);
		getContentPane().add(btnUpdateMovieTrailer);
		
		JButton btnDeleteMovieTrailer = new JButton("Delete MovieTrailer");
		btnDeleteMovieTrailer.setBounds(435, 444, 153, 43);
		getContentPane().add(btnDeleteMovieTrailer);

		
		
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			          },
			new String[] {
				"Id", "Movie", "Duration", "Vid Quality"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("B A C K");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MainInterface maininterface = new MainInterface();
				setVisible(false);
				maininterface.setVisible(true);
				
				
				
				
			}
		});
		btnNewButton_1.setBounds(29, 23, 93, 55);
		getContentPane().add(btnNewButton_1);
		
		JLabel lblMovieTrailers = new JLabel("MOVIE TRAILER");
		lblMovieTrailers.setHorizontalAlignment(SwingConstants.CENTER);
		lblMovieTrailers.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblMovieTrailers.setBounds(170, 11, 376, 41);
		contentPane.add(lblMovieTrailers);
		
		JLabel label = new JLabel("Search");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(381, 90, 69, 25);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(460, 90, 93, 25);
		contentPane.add(textField);
		
	}
}

package swing;

import java.awt.EventQueue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import services.interfaces.CrewMemberDAORemote;
import util.ServiceLocator;
import entities.CrewMember;
public class MovieCrew extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	
	private static CrewMemberDAORemote proxyCrewMember = (CrewMemberDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/CrewMemberDAO!services.interfaces.CrewMemberDAORemote");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MovieCrew frame = new MovieCrew();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MovieCrew() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 550);
		this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(66, 120, 487, 260);
		contentPane.add(scrollPane);
		
		JButton btnAddMovieCrew = new JButton("Add MovieCrew");
		btnAddMovieCrew.setToolTipText("");
		btnAddMovieCrew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AddMovieCrew addmoviecrew = new AddMovieCrew();
				setVisible(false);
				addmoviecrew.setVisible(true);
				
				
				
			}
		});
		btnAddMovieCrew.setBounds(50, 444, 140, 43);
		getContentPane().add(btnAddMovieCrew);

		JButton btnUpdateMovieCrew = new JButton("Update MovieCrew");
		btnUpdateMovieCrew.setBounds(245, 444, 147, 43);
		getContentPane().add(btnUpdateMovieCrew);
		
		JButton btnDeleteMovieCrew = new JButton("Delete MovieCrew");
		btnDeleteMovieCrew.setBounds(445, 444, 136, 43);
		getContentPane().add(btnDeleteMovieCrew);

		
		
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Id", "First_name", "Last_name", "Gender", "Job", "Birthdate"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, String.class, String.class, String.class,
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("B A C K");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MainInterface maininterface = new MainInterface();
				setVisible(false);
				maininterface.setVisible(true);
				
			}
		});
		btnNewButton_1.setBounds(10, 11, 93, 62);
		getContentPane().add(btnNewButton_1);
		
		JLabel lblMovieCrew = new JLabel("MOVIE CREW MEMBERS");
		lblMovieCrew.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblMovieCrew.setBounds(132, 11, 502, 41);
		contentPane.add(lblMovieCrew);
		
		JLabel label = new JLabel("Search");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(379, 90, 69, 25);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(458, 90, 93, 25);
		contentPane.add(textField);
		
	}
}

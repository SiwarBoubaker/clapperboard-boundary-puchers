package swing;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import services.interfaces.CrewMemberDAORemote;
import util.ServiceLocator;
import entities.CrewMember;

public class AddMovieCrew extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	private static CrewMemberDAORemote proxyCrewMember = (CrewMemberDAORemote) ServiceLocator.getInstance().
			getProxy("/partie_kefi_kouki.ejb/CrewMemberDAO!services.interfaces.CrewMemberDAORemote");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddMovieCrew frame = new AddMovieCrew();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddMovieCrew() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 350, 600);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewActor = new JLabel("NEW MOVIE");
		lblNewActor.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblNewActor.setBounds(52, 23, 251, 41);
		getContentPane().add(lblNewActor);
		
		JLabel lblNewLabel = new JLabel("First Name");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 15));
		lblNewLabel.setBounds(58, 163, 84, 29);
		getContentPane().add(lblNewLabel);
		
		JLabel lblGender = new JLabel("Gender");
		lblGender.setFont(new Font("Arial", Font.BOLD, 15));
		lblGender.setBounds(58, 280, 84, 29);
		getContentPane().add(lblGender);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setFont(new Font("Arial", Font.BOLD, 15));
		lblLastName.setBounds(58, 218, 84, 29);
		getContentPane().add(lblLastName);
		
		JLabel lblNationality = new JLabel("Job");
		lblNationality.setFont(new Font("Arial", Font.BOLD, 15));
		lblNationality.setBounds(58, 340, 84, 29);
		getContentPane().add(lblNationality);
		
		JLabel lblBirthdate = new JLabel("Birthdate");
		lblBirthdate.setFont(new Font("Arial", Font.BOLD, 15));
		lblBirthdate.setBounds(58, 399, 84, 29);
		getContentPane().add(lblBirthdate);
		
		JButton btnNewButton = new JButton("SAVE");
		btnNewButton.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButton.setBounds(187, 510, 138, 51);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButtonBack = new JButton("BACK");
		btnNewButtonBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MovieCrew moviecrew = new MovieCrew();
				setVisible(false);
				moviecrew.setVisible(true);
				
				
				
			}
		});
		btnNewButtonBack.setFont(new Font("Stencil", Font.PLAIN, 20));
		btnNewButtonBack.setBounds(10, 510, 138, 51);
		getContentPane().add(btnNewButtonBack);
		
		textField = new JTextField();
		textField.setBounds(186, 164, 106, 29);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(186, 223, 106, 29);
		getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(186, 285, 106, 29);
		getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(186, 345, 106, 29);
		getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(186, 404, 106, 29);
		getContentPane().add(textField_4);
		
		JLabel lblCrewMember = new JLabel("CREW MEMBER");
		lblCrewMember.setHorizontalAlignment(SwingConstants.CENTER);
		lblCrewMember.setFont(new Font("Stencil Std", Font.BOLD, 40));
		lblCrewMember.setBounds(10, 63, 334, 41);
		contentPane.add(lblCrewMember);
	}

}

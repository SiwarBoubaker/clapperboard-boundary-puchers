package swing;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainInterface extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainInterface frame = new MainInterface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 550);
		this.setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("ACTORS");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				ActorSwing actor = new ActorSwing();
				setVisible(false);
				actor.setVisible(true);
				
				
			}
		});
		btnNewButton.setFont(new Font("Bookman Old Style", Font.BOLD, 26));
		btnNewButton.setBounds(48, 56, 260, 86);
		contentPane.add(btnNewButton);
		
		JButton btnMovieTrailer = new JButton("MOVIE TRAILER");
		btnMovieTrailer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				MovieTrailer movietrailer = new MovieTrailer();
				setVisible(false);
				movietrailer.setVisible(true);
				
				
				
			}
		});
		btnMovieTrailer.setFont(new Font("Bookman Old Style", Font.BOLD, 26));
		btnMovieTrailer.setBounds(48, 218, 259, 86);
		contentPane.add(btnMovieTrailer);
		
		JButton btnMovieCrew = new JButton("MOVIE CREW");
		btnMovieCrew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MovieCrew moviecrew = new MovieCrew();
				setVisible(false);
				moviecrew.setVisible(true);
				
				
				
			}
		});
		btnMovieCrew.setFont(new Font("Bookman Old Style", Font.BOLD, 26));
		btnMovieCrew.setBounds(346, 56, 260, 86);
		contentPane.add(btnMovieCrew);
		
		JButton btnSoundTracks = new JButton("SOUND TRACKS");
		btnSoundTracks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				SoundTrack soundtrack = new SoundTrack();
				setVisible(false);
				soundtrack.setVisible(true);
				
				
				
				
				
			}
		});
		btnSoundTracks.setFont(new Font("Bookman Old Style", Font.BOLD, 26));
		btnSoundTracks.setBounds(347, 218, 259, 86);
		contentPane.add(btnSoundTracks);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				setVisible(false);
				
				
			}
		});
		btnExit.setFont(new Font("Bookman Old Style", Font.BOLD, 26));
		btnExit.setBounds(241, 413, 163, 86);
		contentPane.add(btnExit);
	}
}

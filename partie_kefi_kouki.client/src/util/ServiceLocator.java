package util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ServiceLocator {
	private Context context = null;
	private static ServiceLocator instance = null;
	private Map<String, Object> cache = null;

	private ServiceLocator() {
		try {
			context = new InitialContext();
			cache = Collections.synchronizedMap(new HashMap<String, Object>());
		} catch (NamingException e) {
		}
	}

	public static ServiceLocator getInstance() {
		if (instance == null) {
			instance = new ServiceLocator();
		}
		return instance;
	}

	public Object getProxy(String JNDIName) {
		Object proxy = null;
		if (cache.containsKey(JNDIName)) {
			proxy = cache.get(JNDIName);
			System.out.println("Acc�s � partir du cache");
		} else {
			try {
				proxy = context.lookup(JNDIName);
				cache.put(JNDIName, proxy);
				System.out.println("Acc�s � partir du serveur");
			} catch (NamingException e) {
			}
		}
		return proxy;
	}

}

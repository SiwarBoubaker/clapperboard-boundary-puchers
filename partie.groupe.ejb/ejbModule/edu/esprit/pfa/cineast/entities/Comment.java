package edu.esprit.pfa.cineast.entities;

import edu.esprit.pfa.cineast.entities.RegistredUsers;
import java.io.Serializable;
import java.lang.String;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Comment
 *
 */
@Entity

public class Comment implements Serializable {

	
	private int id;
	private String text;
	private Movies movierelated;
	private RegistredUsers owner;
	private Date dateOfposting;
	private static final long serialVersionUID = 1L;

	public Comment() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}  
	@ManyToOne
	public RegistredUsers getOwner() {
		return this.owner;
	}

	public void setOwner(RegistredUsers owner) {
		this.owner = owner;
	}
	@ManyToOne
	public Movies getMovierelated() {
		return movierelated;
	}
	public void setMovierelated(Movies movierelated) {
		this.movierelated = movierelated;
	}
	public Date getDateOfposting() {
		return dateOfposting;
	}
	public void setDateOfposting(Date dateOfposting) {
		this.dateOfposting = dateOfposting;
	}
   
}

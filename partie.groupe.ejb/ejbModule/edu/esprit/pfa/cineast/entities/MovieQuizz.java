package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieQuizz
 *
 */
@Entity

public class MovieQuizz implements Serializable {

	
	private int Id;
	private Movies movies;
	private List<UserMovieQuizz> userMovieQuizzs;
	private String question;
	private String reponse;
	private static final long serialVersionUID = 1L;

	public MovieQuizz() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}
	@ManyToOne(cascade=CascadeType.REMOVE)
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	@OneToMany(mappedBy = "movieQuizz")
	public List<UserMovieQuizz> getUserMovieQuizzs() {
		return userMovieQuizzs;
	}
	public void setUserMovieQuizzs(List<UserMovieQuizz> userMovieQuizzs) {
		this.userMovieQuizzs = userMovieQuizzs;
	}
	
	
	
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getReponse() {
		return reponse;
	}
	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
   
}

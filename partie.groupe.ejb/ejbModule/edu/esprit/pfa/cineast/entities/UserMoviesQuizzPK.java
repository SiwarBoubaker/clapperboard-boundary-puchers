package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UserMoviesQuizzPK
 *
 */
@Embeddable

public class UserMoviesQuizzPK implements Serializable {

	private int UsersId;
	private int Id;
	private static final long serialVersionUID = 1L;

	public UserMoviesQuizzPK() {
		super();
	}
	

	public UserMoviesQuizzPK(int usersId, int id) {
		super();
		UsersId = usersId;
		Id = id;
	}


	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + UsersId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMoviesQuizzPK other = (UserMoviesQuizzPK) obj;
		if (Id != other.Id)
			return false;
		if (UsersId != other.UsersId)
			return false;
		return true;
	}
	
	
   
}

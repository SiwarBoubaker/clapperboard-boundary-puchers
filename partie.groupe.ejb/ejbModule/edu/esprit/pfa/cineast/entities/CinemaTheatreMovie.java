package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CinemaTheatreMovie
 *
 */
@Entity

public class CinemaTheatreMovie implements Serializable {

	private CinemaTheatreMoviePK cinemaTheatreMoviePK;
	private CinemaTheatre cinemaTheatre;
	private Movies movies;
	private static final long serialVersionUID = 1L;

	public CinemaTheatreMovie() {
		super();
	}
@EmbeddedId
	public CinemaTheatreMoviePK getCinemaTheatreMoviePK() {
		return cinemaTheatreMoviePK;
	}

	public void setCinemaTheatreMoviePK(CinemaTheatreMoviePK cinemaTheatreMoviePK) {
		this.cinemaTheatreMoviePK = cinemaTheatreMoviePK;
	}
	
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	public CinemaTheatre getCinemaTheatre() {
		return cinemaTheatre;
	}
	public void setCinemaTheatre(CinemaTheatre cinemaTheatre) {
		this.cinemaTheatre = cinemaTheatre;
	}
	
	
   
}

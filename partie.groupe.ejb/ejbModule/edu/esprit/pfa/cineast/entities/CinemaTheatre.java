package edu.esprit.pfa.cineast.entities;


import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CinemaTheatre
 *
 */
@Entity

public class CinemaTheatre implements Serializable {

	
	private int Id;
	private String nameCinema;
	private String adress;
	private String description;
	private List<Movies> movies ;
	private static final long serialVersionUID = 1L;

	public CinemaTheatre() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getAdress() {
		return this.adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNameCinema() {
		return nameCinema;
	}
	public void setNameCinema(String nameCinema) {
		this.nameCinema = nameCinema;
	}
	@ManyToMany
	@JoinTable(name="CinemaTheatreMoviePK" , joinColumns={@JoinColumn(name="Id")} , inverseJoinColumns={@JoinColumn(name="MovieId")})
	public List<Movies> getMovies() {
		return movies;
	}
	public void setMovies(List<Movies> movies) {
		this.movies = movies;
	}
	
	   
}

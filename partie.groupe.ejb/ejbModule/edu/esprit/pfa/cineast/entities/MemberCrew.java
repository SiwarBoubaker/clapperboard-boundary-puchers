package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieCrew
 *
 */
@Entity

public class MemberCrew extends Participate implements Serializable {

	private String jobs;
	private static final long serialVersionUID = 1L;

	public MemberCrew() {
		super();
	}

	public String getJobs() {
		return jobs;
	}

	public void setJobs(String jobs) {
		this.jobs = jobs;
	}
   
}

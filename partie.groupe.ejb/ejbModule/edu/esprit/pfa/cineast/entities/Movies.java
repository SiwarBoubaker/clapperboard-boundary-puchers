package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.Boolean;
import java.lang.String;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Movies
 *
 */
@Entity

public class Movies implements Serializable {

	
	private int MovieId;
	private String title;
	private Calendar year_released;
	private String description;
	private String genre;
	private String photo;
	private Boolean state_premiere;
	private BoxOffice boxOffice;
	private List<CinemaTheatre> cinemaTheatres;
	private List<MovieQuizz> quiz;
	private List<MoviesUsers> movieUser;
	private List<SoundTrack> soundTracks;
	private MovieTrailerVideo movieTrailerVideo;
	private List<MoviesParticipate> movieParticipate;
	private List<Facts> facts;
	private List<Comment> comments;
	private static final long serialVersionUID = 1L;

	public Movies() {
		super();
	}   
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	 
	public int getMovieId() {
		return MovieId;
	}
	public void setMovieId(int movieId) {
		MovieId = movieId;
	}
	public Calendar getYear_released() {
		return this.year_released;
	}

	public void setYear_released(Calendar year_released) {
		this.year_released = year_released;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getGenre() {
		return this.genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}   
	public String getPhoto() {
		return this.photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}   
	public Boolean getState_premiere() {
		return this.state_premiere;
	}

	public void setState_premiere(Boolean state_premiere) {
		this.state_premiere = state_premiere;
	}
	@ManyToOne(cascade = {CascadeType.ALL,
			CascadeType.REMOVE })
	public BoxOffice getBoxOffice() {
		return boxOffice;
	}
	public void setBoxOffice(BoxOffice boxOffice) {
		this.boxOffice = boxOffice;
	}
	@OneToMany(mappedBy="movies")
	public List<MovieQuizz> getQuiz() {
		return quiz;
	}
	public void setQuiz(List<MovieQuizz> quiz) {
		this.quiz = quiz;
	}
	
	@OneToMany(mappedBy="movies")
	public List<MoviesUsers> getMovieUser() {
		return movieUser;
	}
	public void setMovieUser(List<MoviesUsers> movieUser) {
		this.movieUser = movieUser;
	}
	@OneToMany(mappedBy="movies")
	public List<MoviesParticipate> getMovieParticipate() {
		return movieParticipate;
	}
	public void setMovieParticipate(List<MoviesParticipate> movieParticipate) {
		this.movieParticipate = movieParticipate;
	}

	@OneToMany(mappedBy="movies" , cascade=CascadeType.ALL)
	public List<SoundTrack> getSoundTracks() {
		return soundTracks;
	}

	public void setSoundTracks(List<SoundTrack> soundTracks) {
		this.soundTracks = soundTracks;
	}
 
	public MovieTrailerVideo getMovieTrailerVideo() {
		return movieTrailerVideo;
	}

	public void setMovieTrailerVideo(MovieTrailerVideo movieTrailerVideo) {
		this.movieTrailerVideo = movieTrailerVideo;
	}
	
	

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@OneToMany(mappedBy="movies")
	public List<Facts> getFacts() {
		return facts;
	}

	public void setFacts(List<Facts> facts) {
		this.facts = facts;
	}

	@ManyToMany
	@JoinTable(name="CinemaTheatreMoviePK" , joinColumns={@JoinColumn(name="MovieId")} , inverseJoinColumns={@JoinColumn(name="Id")})

	public List<CinemaTheatre> getCinemaTheatres() {
		return cinemaTheatres;
	}

	public void setCinemaTheatres(List<CinemaTheatre> cinemaTheatres) {
		this.cinemaTheatres = cinemaTheatres;
	}
@OneToMany(mappedBy="movierelated")

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	
	
   
}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Facts
 *
 */
@Entity

public class Facts implements Serializable {

	
	private int idF;
	private String titleF;
	private String description;
	private String UrlPicture;
	private Movies movies;
	private static final long serialVersionUID = 1L;

	public Facts() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getIdF() {
		return this.idF;
	}

	public void setIdF(int idF) {
		this.idF = idF;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public String getUrlPicture() {
		return this.UrlPicture;
	}

	public void setUrlPicture(String UrlPicture) {
		this.UrlPicture = UrlPicture;
	}
	
	public String getTitleF() {
		return titleF;
	}
	public void setTitleF(String titleF) {
		this.titleF = titleF;
	}
	@ManyToOne(cascade=CascadeType.REMOVE)
	//@JoinColumn(insertable="true")
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	
   
}

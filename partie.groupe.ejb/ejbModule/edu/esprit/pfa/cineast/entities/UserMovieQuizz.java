package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: UserMovieQuizz
 *
 */
@Entity

public class UserMovieQuizz implements Serializable {

	private UserMoviesQuizzPK userQuizz;
	private MovieQuizz movieQuizz;
	private static final long serialVersionUID = 1L;

	public UserMovieQuizz() {
		super();
	}
     @EmbeddedId
	public UserMoviesQuizzPK getUserQuizz() {
		return userQuizz;
	}

	public void setUserQuizz(UserMoviesQuizzPK userQuizz) {
		this.userQuizz = userQuizz;
	}
	@ManyToOne
	@JoinColumn(name = "Id", referencedColumnName = "Id", insertable = false, updatable = false)
	public MovieQuizz getMovieQuizz() {
		return movieQuizz;
	}

	public void setMovieQuizz(MovieQuizz movieQuizz) {
		this.movieQuizz = movieQuizz;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((userQuizz == null) ? 0 : userQuizz.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMovieQuizz other = (UserMovieQuizz) obj;
		if (userQuizz == null) {
			if (other.userQuizz != null)
				return false;
		} else if (!userQuizz.equals(other.userQuizz))
			return false;
		return true;
	}
	
	
	
   
}

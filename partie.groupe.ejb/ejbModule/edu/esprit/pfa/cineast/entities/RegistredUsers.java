package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

/**
 * Entity implementation class for Entity: RegistredUsers
 *
 */
@Entity

public class RegistredUsers implements Serializable {
 
	@Lob
	private Byte[] pic; 
	private String email;
	private String telephonNumber;
	private List<Comment> comments;
	private List<RegistredUsersWishList> registredUsersWishLists;
	private int UsersId;
	private int CIN;
	private String name;
	private String surname;
	private String nationality;
	private Date Birthday;
	private String sexe;
	private String login;
	private String password;

	
	
	private static final long serialVersionUID = 1L;

	public RegistredUsers() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}

	public int getCIN() {
		return this.CIN;
	}

	public void setCIN(int CIN) {
		this.CIN = CIN;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	 @Size(min=6)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	@Past
	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@OneToMany(mappedBy="registredUsers")
	public List<RegistredUsersWishList> getRegistredUsersWishLists() {
		return registredUsersWishLists;
	}

	public void setRegistredUsersWishLists(List<RegistredUsersWishList> registredUsersWishLists) {
		this.registredUsersWishLists = registredUsersWishLists;
	}

	

	public String getTelephonNumber() {
		return telephonNumber;
	}

	public void setTelephonNumber(String telephonNumber) {
		this.telephonNumber = telephonNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
@OneToMany(mappedBy="owner")
	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Byte[] getPic() {
		return pic;
	}

	public void setPic(Byte[] pic) {
		this.pic = pic;
	}

   
}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Participate
 * 
 */
@Entity
public abstract class Participate implements Serializable {

	private int Idparticipate;
	private String firstName;
	private String lastName;
	private String birthdate;
	private String gender;
	private List<MoviesParticipate> movieParticipate;
	private static final long serialVersionUID = 1L;

	public Participate() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getIdparticipate() {
		return Idparticipate;
	}

	public void setIdparticipate(int idparticipate) {
		Idparticipate = idparticipate;
	}

	

	@OneToMany(mappedBy = "participate")
	public List<MoviesParticipate> getMovieParticipate() {
		return movieParticipate;
	}

	public void setMovieParticipate(List<MoviesParticipate> movieParticipate) {
		this.movieParticipate = movieParticipate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: SoundTrack
 *
 */
@Entity

public class SoundTrack implements Serializable {

	
	private int Id;
	private String title;
	private String URL;
	private String composer;
	private String duration;
	private Movies movies;
	private static final long serialVersionUID = 1L;

	public SoundTrack() {
		super();
	}   
	@Id    
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getURL() {
		return this.URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}   
	
	@ManyToOne(cascade = { CascadeType.ALL,
			CascadeType.REMOVE })
	public Movies getMovies() {
		return movies;
	}
	public void setMovies(Movies movies) {
		this.movies = movies;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComposer() {
		return composer;
	}
	public void setComposer(String composer) {
		this.composer = composer;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
   
}

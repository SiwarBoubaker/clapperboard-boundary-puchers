package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: BoxOffice
 * 
 */

@Entity
public class BoxOffice implements Serializable {

	private int Id;
	private int weeks;
	private double benifit;
	private int views;

	private List<Movies> movies;
	private static final long serialVersionUID = 1L;

	public BoxOffice() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	@OneToMany(mappedBy = "boxOffice", cascade = CascadeType.ALL)
	public List<Movies> getMovies() {
		return movies;
	}

	public void setMovies(List<Movies> movies) {
		this.movies = movies;
	}

	public double getBenifit() {
		return benifit;
	}

	public void setBenifit(double benifit) {
		this.benifit = benifit;
	}

	public int getViews() {
		return views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public int getWeeks() {
		return weeks;
	}

	public void setWeeks(int weeks) {
		this.weeks = weeks;
	}

	

}

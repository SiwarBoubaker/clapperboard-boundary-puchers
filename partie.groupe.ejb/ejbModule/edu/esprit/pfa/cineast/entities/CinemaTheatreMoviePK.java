package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CinemaTheatreMoviePK
 *
 */
@Embeddable

public class CinemaTheatreMoviePK implements Serializable {

	private int MovieId;
	private int Id;
	private static final long serialVersionUID = 1L;

	public CinemaTheatreMoviePK() {
		super();
	}
	
	public CinemaTheatreMoviePK(int movieId, int id) {
		super();
		MovieId = movieId;
		Id = id;
	}

	public int getMovieId() {
		return MovieId;
	}

	public void setMovieId(int movieId) {
		MovieId = movieId;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Id;
		result = prime * result + MovieId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CinemaTheatreMoviePK other = (CinemaTheatreMoviePK) obj;
		if (Id != other.Id)
			return false;
		if (MovieId != other.MovieId)
			return false;
		return true;
	}

	
	
	
   
}

package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MoviesParticipatePK
 *
 */
@Embeddable

public class MoviesParticipatePK implements Serializable {

	private int MovieId;
	private int Idparticipate;
	
	private static final long serialVersionUID = 1L;

	public MoviesParticipatePK() {
		super();
	}

	public int getMoviePK() {
		return MovieId;
	}

	public void setMoviePK(int moviePK) {
		this.MovieId = moviePK;
	}

	public int getParticipatePK() {
		return Idparticipate;
	}

	public void setParticipatePK(int participatePK) {
		this.Idparticipate = participatePK;
	}

	public MoviesParticipatePK(int moviePK, int participatePK) {
		super();
		this.MovieId = moviePK;
		this.Idparticipate = participatePK;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + MovieId;
		result = prime * result + Idparticipate;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoviesParticipatePK other = (MoviesParticipatePK) obj;
		if (MovieId != other.MovieId)
			return false;
		if (Idparticipate != other.Idparticipate)
			return false;
		return true;
	}
   
	
	
}

package edu.esprit.pfa.cineast.entities;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Admin
 *
 */
@Entity

public class Admin  implements Serializable {

	private int UsersId;
	private int CIN;
	private String name;
	private String surname;
	private String nationality;
	private Date Birthday;
	private String sexe;
	private String login;
	private String password;
	private List<MoviesUsers> movieUser;
	private List<UserMovieQuizz> userMovieQuizz;
	private String telephonNumber;
	private String Email;
	private static final long serialVersionUID = 1L;

	public Admin() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}

	public int getCIN() {
		return this.CIN;
	}

	public void setCIN(int CIN) {
		this.CIN = CIN;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSexe() {
		return this.sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getBirthday() {
		return Birthday;
	}

	public void setBirthday(Date birthday) {
		Birthday = birthday;
	}

	public void setUserMovieQuizz(List<UserMovieQuizz> userMovieQuizz) {
		this.userMovieQuizz = userMovieQuizz;
	}

	public String getTelephonNumber() {
		return telephonNumber;
	}

	public void setTelephonNumber(String telephonNumber) {
		this.telephonNumber = telephonNumber;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	
   
}

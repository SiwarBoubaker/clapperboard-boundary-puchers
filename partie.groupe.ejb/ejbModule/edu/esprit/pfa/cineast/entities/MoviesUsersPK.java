package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MoviesUsersPK
 *
 */
@Embeddable

public class MoviesUsersPK implements Serializable {

	private int UsersId;
	private int MovieId;
	
	private static final long serialVersionUID = 1L;

	public MoviesUsersPK() {
		super();
	}
	

	public MoviesUsersPK(int usersId, int movieId) {
		super();
		UsersId = usersId;
		MovieId = movieId;
	}


	public int getMovieId() {
		return MovieId;
	}

	public void setMovieId(int movieId) {
		MovieId = movieId;
	}

	public int getUsersId() {
		return UsersId;
	}

	public void setUsersId(int usersId) {
		UsersId = usersId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + MovieId;
		result = prime * result + UsersId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MoviesUsersPK other = (MoviesUsersPK) obj;
		if (MovieId != other.MovieId)
			return false;
		if (UsersId != other.UsersId)
			return false;
		return true;
	}

   
}

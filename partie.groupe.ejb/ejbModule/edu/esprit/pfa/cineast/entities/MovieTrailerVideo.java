package edu.esprit.pfa.cineast.entities;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: MovieTrailerVideo
 *
 */
@Entity

public class MovieTrailerVideo implements Serializable {

	private int id;
	private String duration;
	private String URL;
	private String videoQuality;
	private static final long serialVersionUID = 1L;

	public MovieTrailerVideo() {
		super();
	}   
	
	public String getURL() {
		return this.URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}   

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getVideoQuality() {
		return videoQuality;
	}

	public void setVideoQuality(String videoQuality) {
		this.videoQuality = videoQuality;
	}
   
}

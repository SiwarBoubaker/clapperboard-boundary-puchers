package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;


import edu.esprit.pfa.cineast.entities.MovieQuizz;

@Remote
public interface MovieQuizDAORemote {

	public void create(MovieQuizz b);

	public MovieQuizz retrieve(int id);

	public void update(MovieQuizz b);

	public void delete(MovieQuizz b);

	public List<MovieQuizz> retrieveAll();
	
	public MovieQuizz  retrieveByNom(String title);

}

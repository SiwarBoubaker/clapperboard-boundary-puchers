package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.CinemaTheatre;
import edu.esprit.pfa.cineast.entities.MovieQuizz;

/**
 * Session Bean implementation class MovieQuizDAO
 */
@Stateless
@LocalBean
public class MovieQuizDAO implements MovieQuizDAORemote, MovieQuizDAOLocal {

	@PersistenceContext
    EntityManager em;
    public MovieQuizDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(MovieQuizz b) {
		em.persist(b);
		
	}

	@Override
	public MovieQuizz retrieve(int id) {
		
		return em.find(MovieQuizz.class, id);
	}

	@Override
	public void update(MovieQuizz b) {
		em.merge(b);
		
	}

	@Override
	public void delete(MovieQuizz b) {
		MovieQuizz x;
		x=em.find(MovieQuizz.class, b.getId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<MovieQuizz> retrieveAll() {
		TypedQuery<MovieQuizz> query = em.createQuery("select m from MovieQuizz m ",MovieQuizz.class);
		return query.getResultList();
	}
	
	@Override
	public MovieQuizz  retrieveByNom(String title) {
		// TODO Auto-generated method stub
		TypedQuery<MovieQuizz> query = em.createQuery(
				"SELECT a FROM MovieQuizz a WHERE a.question = :title", MovieQuizz.class);
		return query.setParameter("title", title).getSingleResult();
	}

}

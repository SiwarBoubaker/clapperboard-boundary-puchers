package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.BoxOffice;

@Local
public interface BoxOfficeDAOLocal {

	
	public void create(BoxOffice b);

	public BoxOffice retrieve(int id);
	
	public void update(BoxOffice b);
	
	public void delete(BoxOffice b);
	
	public List<BoxOffice> retrieveAll();
	
	public BoxOffice retrieveByPeriod(int period);

}

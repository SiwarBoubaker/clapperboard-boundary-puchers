package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.CinemaTheatre;

@Local
public interface CinemaTheatreDAOLocal {
	
	    public void create(CinemaTheatre b);
		
		public CinemaTheatre retrieve(int id);
		
		public void update(CinemaTheatre b);
		
		public void delete(CinemaTheatre b);
		
		public List<CinemaTheatre> retrieveAll();

		public List<CinemaTheatre> retrieveByName(String title);
		
		public CinemaTheatre retrieveByNom(String title);
}

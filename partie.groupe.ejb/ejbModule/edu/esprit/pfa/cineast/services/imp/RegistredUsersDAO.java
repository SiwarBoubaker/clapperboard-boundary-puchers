package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.RegistredUsers;

/**
 * Session Bean implementation class RegistredUsersDAO
 */
@Stateless
@LocalBean
public class RegistredUsersDAO implements RegistredUsersDAORemote, RegistredUsersDAOLocal {

    @PersistenceContext
    EntityManager em;
    
    public RegistredUsersDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(RegistredUsers b) {
		em.persist(b);
		
	}

	@Override
	public RegistredUsers retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(RegistredUsers.class, id);
	}

	@Override
	public void update(RegistredUsers b) {
		em.merge(b);
		em.flush();
	}

	@Override
	public void delete(RegistredUsers b) {
		RegistredUsers x;
		x=em.find(RegistredUsers.class, b.getUsersId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<RegistredUsers> retrieveAll() {
		TypedQuery<RegistredUsers> query = em.createQuery("select r from RegistredUsers r ",RegistredUsers.class);
		return query.getResultList();
	}
	
	@Override
	public RegistredUsers retrieveByName(String login) {
		// TODO Auto-generated method stub
		TypedQuery<RegistredUsers> query = em.createQuery(
				"SELECT r FROM RegistredUsers r WHERE r.login = :login", RegistredUsers.class);
		return query.setParameter("login", login).getSingleResult();
	}
	
	@Override
	public List<RegistredUsers> retrieveByNom(String name) {
		// TODO Auto-generated method stub
		TypedQuery<RegistredUsers> query = em.createQuery(
				"SELECT r FROM RegistredUsers r WHERE r.name = :name", RegistredUsers.class);
		return query.setParameter("name", name).getResultList();
	}

	@Override
	public RegistredUsers retrieveByEmail(String email) {
		// TODO Auto-generated method stub
		TypedQuery<RegistredUsers> query = em.createQuery(
				"SELECT r FROM RegistredUsers r WHERE r.email = :email", RegistredUsers.class);
		return query.setParameter("email", email).getSingleResult();
	}

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.WishList;

@Local
public interface WishListDAOLocal {
	
	public void create(WishList b);

	public WishList	 retrieve(int id);
	
	public void update(WishList b);
	
	public void delete(WishList b);
	
	public List<WishList> retrieveAll();

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Actor;
import edu.esprit.pfa.cineast.entities.Comment;
import edu.esprit.pfa.cineast.entities.Movies;

@Remote
public interface commentDAORemote {
	

	public void create(Comment b);
	
	public Comment retrieve(int id);
	
	public void update(Comment b);
	
	public void delete(Comment b);
	
	public List<Comment> retrieveAll();

	List<Comment> retrieveByMovierelated(Movies movierelated);


}

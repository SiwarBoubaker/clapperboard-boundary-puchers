package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.BoxOffice;

/**
 * Session Bean implementation class BoxOfficeDAO
 */
@Stateless
@LocalBean
public class BoxOfficeDAO implements BoxOfficeDAORemote, BoxOfficeDAOLocal {

	@PersistenceContext
    EntityManager em;
    public BoxOfficeDAO() {
      
    }
	@Override
	public void create(BoxOffice b) {
		em.persist(b);
		
	}
	@Override
	public BoxOffice retrieve(int id) {
		
		return em.find(BoxOffice.class, id);
	}
	@Override
	public void update(BoxOffice b) {
		em.merge(b);
		
	}
	@Override
	public void delete(BoxOffice b) {
		BoxOffice x;
		x=em.find(BoxOffice.class, b.getId());
		em.remove(x);
		em.flush();
		
	}
	@Override
	public List<BoxOffice> retrieveAll() {
		TypedQuery<BoxOffice> query = em.createQuery("select b from BoxOffice b ",BoxOffice.class);
		return query.getResultList();
	}
	
	@Override
	public BoxOffice retrieveByPeriod(int period) {
		// TODO Auto-generated method stub
		TypedQuery<BoxOffice> query = em.createQuery(
				"SELECT a FROM BoxOffice a WHERE a.period = :period", BoxOffice.class);
		return query.setParameter("period", period).getSingleResult();
	}
    

}

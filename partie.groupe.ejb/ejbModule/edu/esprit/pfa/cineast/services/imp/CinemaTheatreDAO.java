package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.CinemaTheatre;

/**
 * Session Bean implementation class CinemaTheatreDAO
 */
@Stateless
@LocalBean
public class CinemaTheatreDAO implements CinemaTheatreDAORemote, CinemaTheatreDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public CinemaTheatreDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(CinemaTheatre b) {
		em.persist(b);
		
	}

	@Override
	public CinemaTheatre retrieve(int id) {
		return em.find(CinemaTheatre.class, id);
	}

	@Override
	public void update(CinemaTheatre b) {
		em.merge(b);
		
	}

	@Override
	public void delete(CinemaTheatre b) {
		CinemaTheatre x;
		x=em.find(CinemaTheatre.class, b.getId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<CinemaTheatre> retrieveAll() {
		TypedQuery<CinemaTheatre> query = em.createQuery("select a from CinemaTheatre a ",CinemaTheatre.class);
		return query.getResultList();
	}
	
	@Override
	public List<CinemaTheatre> retrieveByName(String title) {
		// TODO Auto-generated method stub
		TypedQuery<CinemaTheatre> query = em.createQuery(
				"SELECT a FROM CinemaTheatre a WHERE a.nameCinema = :title", CinemaTheatre.class);
		return query.setParameter("title", title).getResultList();
	}
	

	@Override
	public CinemaTheatre retrieveByNom(String title) {
		// TODO Auto-generated method stub
		TypedQuery<CinemaTheatre> query = em.createQuery(
				"SELECT a FROM CinemaTheatre a WHERE a.nameCinema = :title", CinemaTheatre.class);
		return query.setParameter("title", title).getSingleResult();
	}

}

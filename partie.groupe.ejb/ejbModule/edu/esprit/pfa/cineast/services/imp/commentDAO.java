package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Comment;
import edu.esprit.pfa.cineast.entities.Facts;
import edu.esprit.pfa.cineast.entities.Movies;



/**
 * Session Bean implementation class commentDAO
 */
@Stateless

public class commentDAO implements commentDAORemote {

    @PersistenceContext 
    EntityManager em; 
    public commentDAO() {
      
    }

	@Override
	public void create(Comment b) {
		  em.persist(b);
		
	}

	@Override
	public Comment retrieve(int id) {
		em.find(Comment.class, id);
		return null;
	}

	@Override
	public void update(Comment b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Comment b) {
		em.remove(b);
		
	}

	@Override
	public List<Comment> retrieveAll() {
		TypedQuery<Comment> query = em.createQuery("select c from Comment C ",Comment.class);
		return query.getResultList();
	}


	@Override
	public List<Comment> retrieveByMovierelated(Movies movierelated) {
		TypedQuery<Comment> query = em.createQuery(
				"SELECT a FROM Comment a WHERE a.movierelated = :movierelated", Comment.class);
		return query.setParameter("movierelated", movierelated).getResultList();
	}



}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.Cineast;

@Local
public interface CineastDAOLocal {
	
	    public void create(Cineast b);
		
		public Cineast retrieve(int id);
		
		public void update(Cineast b);
		
		public void delete(Cineast b);
		
		public List<Cineast> retrieveAll();
		
		public Cineast retrieveByName(String login);
		
		public List<Cineast> retrieveByNom(String nom);

}

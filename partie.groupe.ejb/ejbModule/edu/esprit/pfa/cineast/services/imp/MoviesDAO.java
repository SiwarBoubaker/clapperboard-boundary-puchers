package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Admin;
import edu.esprit.pfa.cineast.entities.Movies;

/**
 * Session Bean implementation class MoviesDAO
 */
@Stateless
@LocalBean
public class MoviesDAO implements MoviesDAORemote, MoviesDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public MoviesDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Movies b) {
		em.persist(b);
		
	}

	@Override
	public Movies retrieve(int id) {
		return em.find(Movies.class, id);
	}

	@Override
	public void update(Movies b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Movies b) {
		em.remove(em.find(Movies.class,b.getMovieId()));
		
	}

	
	public List<Movies>retrieveAll() {

		TypedQuery<Movies> query = em.createQuery("SELECT m FROM Movies m",
				Movies.class);

		return query.getResultList();
	}
	
	
	public List<Movies>retrieveByType(String genre) {
		
		TypedQuery<Movies> query = em.createQuery(
				"SELECT j FROM Movies j WHERE j.genre = :genre",
				Movies.class);

		return query.setParameter("genre", genre).getResultList();
	}
	
	public List<Movies>retrieveByTitle(String title) {
		
		TypedQuery<Movies> query = em.createQuery(
				"SELECT j FROM Movies j WHERE j.title = :title",
				Movies.class);

		return query.setParameter("title", title).getResultList();
	}
}

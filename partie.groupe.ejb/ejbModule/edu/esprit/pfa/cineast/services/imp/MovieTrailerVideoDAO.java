package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.MovieTrailerVideo;
import edu.esprit.pfa.cineast.entities.SoundTrack;

/**
 * Session Bean implementation class MovieTrailerVideoDAO
 */
@Stateless
@LocalBean
public class MovieTrailerVideoDAO implements MovieTrailerVideoDAORemote, MovieTrailerVideoDAOLocal {

	@PersistenceContext
    EntityManager em;
	
	
    public MovieTrailerVideoDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(MovieTrailerVideo b) {
		em.persist(b);
		
	}

	@Override
	public MovieTrailerVideo retrieve(int id) {
		return em.find(MovieTrailerVideo.class, id);
	}

	@Override
	public void update(MovieTrailerVideo b) {
		em.merge(b);
		
	}

	@Override
	public void delete(MovieTrailerVideo b) {
		MovieTrailerVideo x;
		x=em.find(MovieTrailerVideo.class, b.getId());
		em.remove(x);
		em.flush();
	}

	@Override
	public List<MovieTrailerVideo> retrieveAll() {
		TypedQuery<MovieTrailerVideo> query = em.createQuery("select m from MovieTrailerVideo m ",MovieTrailerVideo.class);
		return query.getResultList();
	}
	
	@Override
	public MovieTrailerVideo retrieveByQuality(String quality) {
		// TODO Auto-generated method stub
		TypedQuery<MovieTrailerVideo> query = em.createQuery(
				"SELECT a FROM MovieTrailerVideo a WHERE a.videoQuality = :quality", MovieTrailerVideo.class);
		return query.setParameter("quality", quality).getSingleResult();
	}
	
	
}

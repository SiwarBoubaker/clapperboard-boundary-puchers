package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Actor;


/**
 * Session Bean implementation class ActorDAO
 */
@Stateless
@LocalBean
public class ActorDAO implements ActorDAORemote, ActorDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public ActorDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Actor b) {
		em.persist(b);
		
	}

	@Override
	public Actor retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Actor.class, id);
	}

	@Override
	public void update(Actor b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Actor b) {
		Actor x;
		x=em.find(Actor.class, b.getIdparticipate());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Actor> retrieveAll() {
		TypedQuery<Actor> query = em.createQuery("select a from Actor a ",Actor.class);
		return query.getResultList();
	}
	
	@Override
	public List<Actor> retrieveByFirstName(String firstName) {
		// TODO Auto-generated method stub
		TypedQuery<Actor> query = em.createQuery(
				"SELECT a FROM Actor a WHERE a.firstName = :firstName", Actor.class);
		return query.setParameter("firstName", firstName).getResultList();
	}
	
	@Override
	public Actor retrieveByFirstNam(String firstName) {
		// TODO Auto-generated method stub
		TypedQuery<Actor> query = em.createQuery(
				"SELECT a FROM Actor a WHERE a.firstName = :firstName", Actor.class);
		return query.setParameter("firstName", firstName).getSingleResult();
	}
	
	@Override
	public List<Actor> retrieveBylastName(String lastName) {
		// TODO Auto-generated method stub
		TypedQuery<Actor> query = em.createQuery(
				"SELECT a FROM Actor a WHERE a.lastName = :lastName", Actor.class);
		return query.setParameter("lastName", lastName).getResultList();
	}
	
	@Override
	public List<Actor> retrieveBynationality(String nationality) {
		// TODO Auto-generated method stub
		TypedQuery<Actor> query = em.createQuery(
				"SELECT a FROM Actor a WHERE a.nationality = :nationality", Actor.class);
		return query.setParameter("nationality", nationality).getResultList();
	}
	

}

package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.MemberCrew;


@Remote
public interface MovieCrewDAORemote {

    public void create(MemberCrew b);
	
	public MemberCrew retrieve(int id);
	
	public void update(MemberCrew b);
	
	public void delete(MemberCrew b);
	
	public List<MemberCrew> retrieveAll();
	
	public List<MemberCrew> retrieveBylastName(String lastName);
	
	public List<MemberCrew> retrieveByFirstName(String firstName);

}

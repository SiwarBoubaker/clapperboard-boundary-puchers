package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Admin;
import edu.esprit.pfa.cineast.entities.Facts;

/**
 * Session Bean implementation class FactsDAO
 */
@Stateless
@LocalBean
public class FactsDAO implements FactsDAORemote, FactsDAOLocal {

    @PersistenceContext
    EntityManager em;
    
    public FactsDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Facts f) {
		
		em.persist(f);
		
	}

	@Override
	public Facts retrieve(int idF) {
		
		return em.find(Facts.class, idF);
	}

	@Override
	public void update(Facts f) {
		em.merge(f);
		
	}

	@Override
	public void delete(Facts f) {
		Facts x;
		x=em.find(Facts.class, f.getIdF());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Facts> retrieveAll() {
		TypedQuery<Facts> query = em.createQuery("select f from Facts f ",Facts.class);
		return query.getResultList();
	}
	
	@Override
	public Facts retrieveByName(String title) {
		// TODO Auto-generated method stub
		TypedQuery<Facts> query = em.createQuery(
				"SELECT a FROM Facts a WHERE a.titleF = :title", Facts.class);
		return query.setParameter("title", title).getSingleResult();
	}

}

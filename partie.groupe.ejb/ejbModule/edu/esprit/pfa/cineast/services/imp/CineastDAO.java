package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Cineast;

/**
 * Session Bean implementation class CineastDAO
 */
@Stateless
@LocalBean
public class CineastDAO implements CineastDAORemote, CineastDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public CineastDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Cineast b) {
		em.persist(b);
		
	}

	@Override
	public Cineast retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Cineast.class, id);
	}

	@Override
	public void update(Cineast b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Cineast b) {
		Cineast x;
		x=em.find(Cineast.class, b.getUsersId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Cineast> retrieveAll() {
		TypedQuery<Cineast> query = em.createQuery("select c from Cineast c ",Cineast.class);
		return query.getResultList();
	}
	
	@Override
	public Cineast retrieveByName(String login) {
		// TODO Auto-generated method stub
		TypedQuery<Cineast> query = em.createQuery(
				"SELECT a FROM Cineast a WHERE a.login = :login", Cineast.class);
		return query.setParameter("login", login).getSingleResult();
	}
	
	@Override
	public List<Cineast> retrieveByNom(String nom) {
		// TODO Auto-generated method stub
		TypedQuery<Cineast> query = em.createQuery(
				"SELECT a FROM Cineast a WHERE a.name = :nom", Cineast.class);
		return query.setParameter("nom", nom).getResultList();
	}

}

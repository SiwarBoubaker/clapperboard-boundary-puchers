package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.CinemaTheatre;

@Remote
public interface CinemaTheatreDAORemote {
	
    public void create(CinemaTheatre b);
	
	public CinemaTheatre retrieve(int id);
	
	public void update(CinemaTheatre b);
	
	public void delete(CinemaTheatre b);

	public List<CinemaTheatre> retrieveAll();
	
	public List<CinemaTheatre> retrieveByName(String title);
	
	public CinemaTheatre retrieveByNom(String title);
	
}

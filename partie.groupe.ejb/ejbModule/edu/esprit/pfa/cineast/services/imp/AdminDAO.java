package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Admin;

/**
 * Session Bean implementation class AdminDAO
 */
@Stateless
@LocalBean
public class AdminDAO implements AdminDAORemote, AdminDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public AdminDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Admin b) {
		em.persist(b);
		
	}

	@Override
	public Admin retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Admin.class, id);
	}

	@Override
	public void update(Admin b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Admin b) {
		Admin x;
		x=em.find(Admin.class, b.getUsersId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Admin> retrieveAll() {
		TypedQuery<Admin> query = em.createQuery("select a from Admin a ",Admin.class);
		return query.getResultList();
	}
	
	@Override
	public Admin retrieveByName(String login) {
		// TODO Auto-generated method stub
		TypedQuery<Admin> query = em.createQuery(
				"SELECT a FROM Admin a WHERE a.login = :login", Admin.class);
		return query.setParameter("login", login).getSingleResult();
	}
	
	
	@Override
	public List<Admin> retrieveByNom(String name) {
		// TODO Auto-generated method stub
		TypedQuery<Admin> query = em.createQuery(
				"SELECT a FROM Admin a WHERE a.name = :name", Admin.class);
		return query.setParameter("name", name).getResultList();
	}
	
}

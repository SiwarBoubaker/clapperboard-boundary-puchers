package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.Movies;

@Local
public interface MoviesDAOLocal {
	
    public void create(Movies b);
	
	public Movies retrieve(int id);
	
	public void update(Movies b);
	
	public void delete(Movies b);
	
	public List<Movies> retrieveAll();
	
	public List<Movies> retrieveByTitle(String title);
	public List<Movies> retrieveByType(String genre);

}

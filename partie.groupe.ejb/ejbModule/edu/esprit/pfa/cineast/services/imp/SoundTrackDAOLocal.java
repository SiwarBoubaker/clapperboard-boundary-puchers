package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;


import edu.esprit.pfa.cineast.entities.SoundTrack;

@Local
public interface SoundTrackDAOLocal {
	
	    public void create(SoundTrack b);
		
		public SoundTrack retrieve(int id);
		
		public void update(SoundTrack b);
		
		public void delete(SoundTrack b);

		public List<SoundTrack> retrieveAll();
		public SoundTrack retrieveByName(String title);
		public List<SoundTrack> retrieveByTitle(String title);
		public List<SoundTrack> retrieveByComposer(String title);

}

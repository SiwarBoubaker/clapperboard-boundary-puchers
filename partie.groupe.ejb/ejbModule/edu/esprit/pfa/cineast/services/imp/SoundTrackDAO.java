package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.Admin;
import edu.esprit.pfa.cineast.entities.MovieTrailerVideo;
import edu.esprit.pfa.cineast.entities.SoundTrack;

/**
 * Session Bean implementation class SoundTrackDAO
 */
@Stateless
@LocalBean
public class SoundTrackDAO implements SoundTrackDAORemote, SoundTrackDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public SoundTrackDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(SoundTrack b) {
		em.persist(b);
		
	}

	@Override
	public SoundTrack retrieve(int id) {
		
		return em.find(SoundTrack.class, id);
	}

	@Override
	public void update(SoundTrack b) {
		em.merge(b);
		
	}

	@Override
	public void delete(SoundTrack b) {
		SoundTrack x;
		x=em.find(SoundTrack.class, b.getId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<SoundTrack> retrieveAll() {
		TypedQuery<SoundTrack> query = em.createQuery("select s from SoundTrack s ",SoundTrack.class);
		return query.getResultList();
	}
	
	@Override
	public SoundTrack retrieveByName(String title) {
		// TODO Auto-generated method stub
		TypedQuery<SoundTrack> query = em.createQuery(
				"SELECT a FROM SoundTrack a WHERE a.title = :title", SoundTrack.class);
		return query.setParameter("title", title).getSingleResult();
	}

	@Override
	public List<SoundTrack> retrieveByTitle(String title) {
		// TODO Auto-generated method stub
		TypedQuery<SoundTrack> query = em.createQuery(
				"SELECT a FROM SoundTrack a WHERE a.title = :title", SoundTrack.class);
		return query.setParameter("title", title).getResultList();
	}
	
	

	@Override
	public List<SoundTrack> retrieveByComposer(String title) {
		// TODO Auto-generated method stub
		TypedQuery<SoundTrack> query = em.createQuery(
				"SELECT a FROM SoundTrack a WHERE a.composer = :title", SoundTrack.class);
		return query.setParameter("title", title).getResultList();
	}

}

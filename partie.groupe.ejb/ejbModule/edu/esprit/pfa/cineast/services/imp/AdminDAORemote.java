package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Remote;

import edu.esprit.pfa.cineast.entities.Admin;

@Remote
public interface AdminDAORemote {
	
	    public void create(Admin b);
		
		public Admin retrieve(int id);
		
		public void update(Admin b);
		
		public void delete(Admin b);

		public List<Admin> retrieveAll();
		
		public Admin retrieveByName(String login);
		
		public List<Admin> retrieveByNom(String name);
}

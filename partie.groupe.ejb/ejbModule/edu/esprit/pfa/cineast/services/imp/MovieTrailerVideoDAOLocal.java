package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.Local;

import edu.esprit.pfa.cineast.entities.MovieTrailerVideo;

@Local
public interface MovieTrailerVideoDAOLocal {
	
	    public void create(MovieTrailerVideo b);
		
		public MovieTrailerVideo retrieve(int id);
		
		public void update(MovieTrailerVideo b);
		
		public void delete(MovieTrailerVideo b);

		public List<MovieTrailerVideo> retrieveAll();
		
		public MovieTrailerVideo retrieveByQuality(String quality);

}

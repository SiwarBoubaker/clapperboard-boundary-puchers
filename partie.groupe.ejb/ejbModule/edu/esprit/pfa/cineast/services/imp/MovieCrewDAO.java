package edu.esprit.pfa.cineast.services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import edu.esprit.pfa.cineast.entities.MemberCrew;

/**
 * Session Bean implementation class MovieCrewDAO
 */
@Stateless
@LocalBean
public class MovieCrewDAO implements MovieCrewDAORemote, MovieCrewDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public MovieCrewDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(MemberCrew b) {
		em.persist(b);
		
	}

	@Override
	public MemberCrew retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(MemberCrew.class, id);
	}

	@Override
	public void update(MemberCrew b) {
		em.merge(b);
		
	}

	@Override
	public void delete(MemberCrew b) {
		MemberCrew x;
		x=em.find(MemberCrew.class, b.getIdparticipate());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<MemberCrew> retrieveAll() {
		TypedQuery<MemberCrew> query = em.createQuery("select m from MovieCrew m ",MemberCrew.class);
		return query.getResultList();
	}
	
	@Override
	public List<MemberCrew> retrieveByFirstName(String firstName) {
		// TODO Auto-generated method stub
		TypedQuery<MemberCrew> query = em.createQuery(
				"SELECT a FROM MovieCrew a WHERE a.firstName = :firstName", MemberCrew.class);
		return query.setParameter("firstName", firstName).getResultList();
	}
	
	@Override
	public List<MemberCrew> retrieveBylastName(String lastName) {
		// TODO Auto-generated method stub
		TypedQuery<MemberCrew> query = em.createQuery(
				"SELECT a FROM MovieCrew a WHERE a.lastName = :lastName", MemberCrew.class);
		return query.setParameter("lastName", lastName).getResultList();
	}

}

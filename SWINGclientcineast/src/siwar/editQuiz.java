package siwar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;

public class editQuiz extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnAddQuestion;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					editQuiz frame = new editQuiz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public editQuiz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 83, 472, 419);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		DefaultTableModel md = new DefaultTableModel(); 
		md.setColumnIdentifiers(new String[]{"id","Text Of Question"}); 
		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("select idQ ,libelle from question ");
			
			while (rs.next()) {
					md.addRow(new Object[] { rs.getInt("idQ"), rs.getString("libelle") });	
			}
			
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}
		table.setModel(md);
		
		btnAddQuestion = new JButton("Add Question");
		btnAddQuestion.setBounds(599, 80, 142, 23);
		contentPane.add(btnAddQuestion);
		
		btnNewButton = new JButton("Delete Question");
		btnNewButton.setBounds(599, 125, 142, 23);
		contentPane.add(btnNewButton);
	}
}

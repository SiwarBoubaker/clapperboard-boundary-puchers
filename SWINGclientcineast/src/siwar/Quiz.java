package siwar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Quiz extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Quiz frame = new Quiz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	

	public Quiz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(68, 126, 431, 349);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
	
		
		DefaultTableModel md = new DefaultTableModel(); 
		md.setColumnIdentifiers(new String[] {"Id","Number Question", "Title"});
		
		
		
		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

		} catch (ClassNotFoundException e) {
			System.out.println("enable to lead driver class");
			return;
		}
	
	
		
		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("select id, numberQuestion,title  from quizz");
			
			while (rs.next()) {
					md.addRow(new Object[] { rs.getInt("id"), rs.getInt("numberQuestion"),rs.getString("title") });	
			}
			
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}
		
		table.setRowSelectionAllowed(isCursorSet());
		table.setModel(md);
		
		JButton btnback = new JButton("Back");
		btnback.setBounds(43, 31, 89, 23);
		contentPane.add(btnback);
		
		JButton btnlogout = new JButton("Log Out");
		btnlogout.setBounds(630, 31, 89, 23);
		contentPane.add(btnlogout);
		
		JButton btnNewButton = new JButton("Add Quiz");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				addQuizz a = new addQuizz(); 
				a.setVisible(true); 
			}
		});
		btnNewButton.setBounds(591, 123, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Delete Quiz");
		btnNewButton_1.setBounds(591, 195, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Edit Quiz");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				editQuiz q = new editQuiz(); 
				q.setVisible(true); 
				
			}
		});
		btnNewButton_2.setBounds(591, 276, 89, 23);
		contentPane.add(btnNewButton_2);
	}
}

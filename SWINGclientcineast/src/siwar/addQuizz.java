package siwar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;

import edu.esprit.pfa.cineast.entities.Movies;
import edu.esprit.pfa.cineast.services.imp.MoviesDAORemote;
import edu.esprit.pfa.cineast.services.imp.QuizDAORemote;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class addQuizz extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldtitle;
	private JTextField textFieldnumber;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addQuizz frame = new addQuizz();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public addQuizz() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textFieldtitle = new JTextField();
		textFieldtitle.setBounds(174, 52, 86, 20);
		contentPane.add(textFieldtitle);
		textFieldtitle.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Title");
		lblNewLabel.setBounds(58, 55, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Number of Question");
		lblNewLabel_1.setBounds(24, 119, 115, 14);
		contentPane.add(lblNewLabel_1);
		
		textFieldnumber = new JTextField();
		textFieldnumber.setBounds(174, 116, 86, 20);
		contentPane.add(textFieldnumber);
		textFieldnumber.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Movie Related");
		lblNewLabel_2.setBounds(24, 183, 102, 14);
		contentPane.add(lblNewLabel_2);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setBounds(174, 180, 86, 20);
		contentPane.add(comboBox);
		
		JButton btnNewButton = new JButton("add to the list");
		btnNewButton.addMouseListener(new MouseAdapter() {
			
			public void mouseClicked(MouseEvent e) {
				
				

				QuizDAORemote u;
				try {
					InitialContext context; 
					context = new InitialContext();
					u =  (QuizDAORemote) context.lookup("ejb:/edu.esprit.pfa.cineast/QuizDAO!edu.esprit.pfa.cineast.services.imp.QuizDAORemote");

					edu.esprit.pfa.cineast.entities.Quiz q = null;
					q.setMovierelated((Movies) comboBox.getSelectedItem());
					q.setTitle(textFieldtitle.getText()); 
					int number= Integer.parseInt(textFieldnumber.getText());
					q.setNumberquestion(number); 
					u.create(q);
					
					
				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(119, 253, 123, 23);
		contentPane.add(btnNewButton);
		
		Vector items = new Vector();
		DefaultComboBoxModel md1 = new DefaultComboBoxModel(items);
		
		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

		} catch (ClassNotFoundException e) {
			System.out.println("enable to lead driver class");
			return;
		}

		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title from movies");

			while (rs.next()) {
				 
					items.add(rs.getString("title"));
			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}
	comboBox.setModel(md1); 
	
	
	}
	
}

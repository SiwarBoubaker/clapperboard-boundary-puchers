package siwar;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import edu.esprit.pfa.cineast.entities.Movies;
import edu.esprit.pfa.cineast.services.imp.MoviesDAORemote;

public class BoxOffice extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox combomovies;
	private JTextField textFielddate;
	private JTextField textFieldben;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					BoxOffice frame = new BoxOffice();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BoxOffice() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 92, 495, 368);
		contentPane.add(scrollPane);
		int idmov;
		float benifitsmov;
		final String movititle;
		
		table.setRowSelectionAllowed(isCursorSet());
		final DefaultTableModel md = new DefaultTableModel();
		md.setColumnIdentifiers(new String[] { "Id", "Title", "Benifits" });
		
		
		Vector items = new Vector();
		DefaultComboBoxModel md1 = new DefaultComboBoxModel(items);
		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

		} catch (ClassNotFoundException e) {
			System.out.println("enable to lead driver class");
			return;
		}

		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,boxOffice from movies");

			while (rs.next()) {
				 
					items.add(rs.getString("title"));
			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}

		combomovies = new JComboBox(md1);
		combomovies.setBounds(614, 92, 126, 29);
		contentPane.add(combomovies);
		movititle = (String) combomovies.getSelectedItem();

		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,movieid,benifits from movies where title="
							+ movititle);

			while (rs.next()) {
				
				idmov=rs.getInt("movieid");
				benifitsmov=rs.getFloat("benifits");
				
			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}

		table = new JTable();
		scrollPane.setViewportView(table);

		// remplissage du tableau automatiquement

	
	{
		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,boxOffice,benifits,movieid from movies order by benifits desc ");

			while (rs.next()) {
				if (rs.getInt("boxOffice") == 1) {
					md.addRow(new Object[] { rs.getInt("movieid"),
							rs.getString(1), rs.getFloat(3) });
				}

			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}
		
	}
	
		
		JButton btnAddMovie = new JButton("Add Movie ");
		btnAddMovie.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				int i = table.getSelectedRow(); 
				int a= (Integer) table.getValueAt(i, 0);
				
			
				MoviesDAORemote u;
				try {
					InitialContext context; 
					context = new InitialContext();
					u =  (MoviesDAORemote) context.lookup("/edu.esprit.pfa.cineast/MoviesDAO!edu.esprit.pfa.cineast.services.imp.MoviesDAORemote");
					
					
				Movies m= u.retrieve(a);
				
				m.setBoxOffice(true);
				u.update(m);
					
				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}
		});
		btnAddMovie.setBounds(614, 145, 126, 23);
		contentPane.add(btnAddMovie);
		table.setModel(md);

		

		JButton btnDeleteMovie = new JButton("Delete Movie");
		btnDeleteMovie.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				MoviesDAORemote u;
				Properties props = new Properties();
	            try {
					props.load(new FileInputStream("jndi.properties"));
				} catch (FileNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				try {
					InitialContext context; 
					context = new InitialContext();
					u =  (MoviesDAORemote) context.lookup("ejb:/edu.esprit.pfa.cineast/MoviesDAO!edu.esprit.pfa.cineast.services.imp.MoviesDAORemote");

					Movies m= new Movies();
				m.setBoxOffice(false); 
				u.update(m);
					
				} catch (NamingException e1) {
				
					e1.printStackTrace();
				}
			
			}
				
			
		});
		btnDeleteMovie.setBounds(614, 179, 126, 23);
		contentPane.add(btnDeleteMovie);


		

		textFielddate = new JTextField();
		textFielddate.setBounds(614, 336, 126, 20);
		contentPane.add(textFielddate);
		textFielddate.setColumns(10);
		DateFormat dateform= new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar c1= Calendar.getInstance(); 
		
		String d1= dateform.format(c1.getTime());
		System.out.print(d1);
		textFielddate.setText(d1);
	
		JLabel lblNewLabel = new JLabel("Date");
		lblNewLabel.setBounds(614, 302, 109, 23);
		contentPane.add(lblNewLabel);

		JButton btnBack = new JButton("Back");
		btnBack.setBounds(40, 22, 89, 23);
		contentPane.add(btnBack);

		JButton btnLogOUt = new JButton("Log Out");
		btnLogOUt.setBounds(634, 22, 89, 23);
		contentPane.add(btnLogOUt);
		
		textFieldben = new JTextField();
		textFieldben.setBounds(614, 237, 126, 20);
		contentPane.add(textFieldben);
		textFieldben.setColumns(10);
		table.getColumnModel().getColumn(2).setPreferredWidth(0);
		JButton btnNewButton = new JButton("Update Benifits");
		btnNewButton.addActionListener(new ActionListener() {
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int i = table.getSelectedRow(); 
				int a= (Integer) table.getValueAt(i, 0);
				
				String benifit = textFieldben.getText(); 
				float ben = Float.parseFloat(benifit); 
				System.out.print(ben);
				
				MoviesDAORemote u;
				try {
					InitialContext context; 
					context = new InitialContext();
					
					u =  (MoviesDAORemote) context.lookup("/edu.esprit.pfa.cineast/MoviesDAO!edu.esprit.pfa.cineast.services.imp.MoviesDAORemote");
					
					Movies m= u.retrieve(a);
					
				m.setBenifits(ben);
				u.update(m);
					
				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		
				
				
				
			}
		});
		btnNewButton.setBounds(614, 268, 126, 23);
		contentPane.add(btnNewButton);
	}
	public JTextField getTextFieldben() {
		return textFieldben;
	}
}

package siwar;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.spi.InitialContextFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.GridBagLayout;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.JScrollPane;

import edu.esprit.pfa.cineast.entities.Movies;
import edu.esprit.pfa.cineast.services.imp.BoxOfficeDAO;
import edu.esprit.pfa.cineast.services.imp.BoxOfficeDAORemote;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BoxOffice extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JComboBox combomovies;
	private JTextField textFielddate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BoxOffice frame = new BoxOffice();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BoxOffice() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(40, 92, 495, 368);
		contentPane.add(scrollPane);
		int idmov;
		float benifitsmov;
		final String movititle;
		
		
		final DefaultTableModel md = new DefaultTableModel();
		md.setColumnIdentifiers(new String[] { "Id", "Title", "Benifits" });
		
		
		Vector items = new Vector();
		DefaultComboBoxModel md1 = new DefaultComboBoxModel(items);
		
		// chargement du pilote de la base de donn�e
		try {
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");

		} catch (ClassNotFoundException e) {
			System.out.println("enable to lead driver class");
			return;
		}

		// ouverture d'une connexion et remplissage du combobox 
		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,boxOffice_id from movies");

			while (rs.next()) {
				 
					items.add(rs.getString("title"));
				

			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}

		combomovies = new JComboBox(md1);
		combomovies.setBounds(614, 92, 126, 29);
		contentPane.add(combomovies);
		movititle = (String) combomovies.getSelectedItem();
		
		// ouverture d'une deuxieme connexion (l'ancienne connexion servant a remplir le combo box a �t� 

		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,movieid,benifits from movies where title="
							+ movititle);

			while (rs.next()) {
				
				idmov=rs.getInt("movieid");
				benifitsmov=rs.getFloat("benifits");
				
			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}

		table = new JTable();
		scrollPane.setViewportView(table);

		// remplissage du tableau automatiquement

	
	{
		try {
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/cineast", "root", "admin");

			Statement sql = con.createStatement();
			ResultSet rs = sql
					.executeQuery("SELECT title,boxOffice_id,benifits,movieid from movies order by benifits desc ");

			while (rs.next()) {
				if (rs.getInt("boxOffice_id") == 1) {
					md.addRow(new Object[] { rs.getInt("movieid"),
							rs.getString(1), rs.getFloat(3) });
				}

			}
			rs.close();
			sql.close();
			con.close();

		} catch (SQLException sc) {
			System.out.println("sql exception");
		}
		
	}
		
		
		JButton btnAddMovie = new JButton("Add Movie ");
		btnAddMovie.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Connection con = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/cineast", "root", "admin");

					Statement sql = con.createStatement();
					ResultSet rs = sql
							.executeQuery("UPDATE movies SET boxOffice_id ='1' WHERE title = \""+movititle+"");

					
					rs.close();
					sql.close();
					con.close();

				} catch (SQLException sc) {
					System.out.println("sql exception");}
			
			}
		});
		btnAddMovie.setBounds(614, 145, 126, 23);
		contentPane.add(btnAddMovie);
		table.setModel(md);

		

		JButton btnDeleteMovie = new JButton("Delete Movie");
		btnDeleteMovie.setBounds(614, 179, 126, 23);
		contentPane.add(btnDeleteMovie);

		JButton btnNewButton = new JButton("Update Benifits");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton.setBounds(614, 223, 126, 23);
		contentPane.add(btnNewButton);

		textFielddate = new JTextField();
		textFielddate.setBounds(614, 336, 126, 20);
		contentPane.add(textFielddate);
		textFielddate.setColumns(10);

		JLabel lblNewLabel = new JLabel("Date");
		lblNewLabel.setBounds(614, 302, 109, 23);
		contentPane.add(lblNewLabel);

		JButton btnBack = new JButton("Back");
		btnBack.setBounds(40, 22, 89, 23);
		contentPane.add(btnBack);

		JButton btnLogOUt = new JButton("Log Out");
		btnLogOUt.setBounds(634, 22, 89, 23);
		contentPane.add(btnLogOUt);
		table.getColumnModel().getColumn(2).setPreferredWidth(0);

	}
}

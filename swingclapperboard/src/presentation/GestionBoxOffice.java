package src.presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.dyno.visual.swing.layouts.Bilateral;
import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;
import org.dyno.visual.swing.layouts.Trailing;


//VS4E -- DO NOT REMOVE THIS LINE!
public class GestionBoxOffice extends JFrame {
int b; 
	private static final long serialVersionUID = 1L;
	private JTable tablefilm;
	private JScrollPane scroll1;
	private JButton deletemovie;
	private JLabel jLabel0;
	private JButton addmov;
	private JLabel jLabel1;
	private JTextField moviefield;
	private JPanel jPanel1;
	private JPanel jPanel0;
	private JButton jButton2;
	private JButton BackButton;
	private JButton rating;
	private JComboBox jComboBox1;
	private JTable jTable0;
	private JButton jButton5;
	private static final String PREFERRED_LOOK_AND_FEEL = "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel";
	public GestionBoxOffice() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getJButton2(), new Constraints(new Leading(492, 89, 12, 12), new Leading(9, 23, 12, 12)));
		add(getJButton5(), new Constraints(new Leading(16, 10, 10), new Leading(6, 6, 6)));
		add(getJPanel0(), new Constraints(new Leading(16, 565, 6, 6), new Leading(38, 257, 10, 10)));
		setSize(588, 306);
	}

	private JButton getJButton5() {
		if (jButton5 == null) {
			jButton5 = new JButton();
			jButton5.setText("Back");
		}
		return jButton5;
	}

	private JTable getTablefilm() {
		if (tablefilm == null) {
			tablefilm = new JTable();
			tablefilm.setModel(new DefaultTableModel(new Object[][] { { "null", " ", }, }, new String[] { "Title  ", "Rank ", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
			tablefilm.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
				
				}
			});
		}
		return tablefilm;
	}

	private JScrollPane getScroll1() {
		if (scroll1 == null) {
			scroll1 = new JScrollPane();
			scroll1.setViewportView(getJTable0());
		}
		return scroll1;
	}

	private JButton getDeletemovie() {
		if (deletemovie == null) {
			deletemovie = new JButton();
			deletemovie.setText("Delete");
			deletemovie.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					deletemovieMouseMouseClicked(event);
				}
			});
		}
		return deletemovie;
	}

	private JComboBox getJComboBox1() {
		if (jComboBox1 == null) {
			jComboBox1 = new JComboBox();
			jComboBox1.setModel(new DefaultComboBoxModel(new Object[] {}));
			jComboBox1.setDoubleBuffered(false);
			jComboBox1.setBorder(null);
		}
		return jComboBox1;
	}

	private JButton getJButton4() {
		if (rating == null) {
			rating = new JButton();
			rating.setText("Modify Rank");
		}
		return rating;
	}

	private JButton getJButton3() {
		if (BackButton == null) {
			BackButton = new JButton();
			BackButton.setText("Back");
			BackButton.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton3MouseMouseClicked(event);
				}
			});
		}
		return BackButton;
	}

	private JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new JButton();
			jButton2.setText("Log Out");
			jButton2.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton2ActionActionPerformed(event);
				}
			});
			jButton2.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
				}
			});
		}
		return jButton2;
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setBorder(BorderFactory.createTitledBorder(null, "Box Office", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, new Font("Dialog", Font.BOLD, 12),
					new Color(51, 51, 51)));
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJPanel1(), new Constraints(new Leading(328, 171, 10, 10), new Leading(16, 141, 12, 12)));
			jPanel0.add(getDeletemovie(), new Constraints(new Leading(247, 75, 6, 6), new Leading(20, 23, 10, 10)));
			jPanel0.add(getJComboBox1(), new Constraints(new Leading(0, 46, 6, 6), new Trailing(0, 25, 48, 169)));
			jPanel0.add(getScroll1(), new Constraints(new Leading(-4, 249, 10, 10), new Leading(7, 175, 10, 10)));
		}
		return jPanel0;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Update", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, new Font("Dialog", Font.BOLD, 12),
					new Color(51, 51, 51)));
			jPanel1.setLayout(new GroupLayout());
			jPanel1.add(getJLabel0(), new Constraints(new Leading(7, 10, 10), new Leading(0, 12, 12)));
			jPanel1.add(getJTextField0(), new Constraints(new Bilateral(62, 12, 4), new Leading(-2, 12, 12)));
			jPanel1.add(getJLabel1(), new Constraints(new Leading(7, 55, 10, 10), new Leading(40, 19, 10, 10)));
			jPanel1.add(getJButton1(), new Constraints(new Leading(-4, 10, 10), new Leading(77, 22, 6, 6)));
		}
		return jPanel1;
	}

	private JTextField getJTextField0() {
		if (moviefield == null) {
			moviefield = new JTextField();
		}
		return moviefield;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("Benifits ");
		}
		return jLabel1;
	}

	private JButton getJButton1() {
		if (addmov == null) {
			addmov = new JButton();
			addmov.setText("Confirm");
		}
		return addmov;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Movie");
		}
		return jLabel0;
	}

	private JButton getJButton0() {
		if (deletemovie == null) {
			deletemovie = new JButton();
			deletemovie.setText("Delete");
		}
		return deletemovie;
	}

	private JScrollPane getJScrollPane0() {
		if (scroll1 == null) {
			scroll1 = new JScrollPane();
			scroll1.setViewportView(getJTable0());
		}
		return scroll1;
	}

	private JTable getJTable0() {
		if (jTable0 == null) {
			jTable0 = new JTable();
			jTable0.setModel(new DefaultTableModel(new Object[][] { { 0, 0, "0x2", "0x3", }, { 0, 0, "1x2", "1x3", }, { 0, 0, "2x2", "2x3", },
					{ 0, 0, "3x2", "3x3", }, { 0, 0, "4x2", "4x3", }, }, new String[] { "Id", "Rank ", "Title  ", "Benifits", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Integer.class, Integer.class, String.class, String.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
			jTable0.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					tablefilmMouseMouseClicked(event);
				}
			});
		}
		return jTable0;
	}

	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			if (lnfClassname == null)
				lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GestionBoxOffice frame = new GestionBoxOffice();
				frame.setDefaultCloseOperation(GestionBoxOffice.EXIT_ON_CLOSE);
				frame.setTitle("GestionBoxOffice");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private void jButton2ActionActionPerformed(ActionEvent event) {
		Auth au =new Auth();
		this.setVisible(false);
		au.setVisible(true);
	}

	private void jButton3MouseMouseClicked(MouseEvent event) {
		this.dispose(); 
		
	}

	private void deletemovieMouseMouseClicked(MouseEvent event) {
int x; 
int i; 
x= tablefilm.getSelectedRow(); 
tablefilm.getValueAt(x, 1);
tablefilm.getSelectionModel().addListSelectionListener(tablefilm);
	 
	tablefilm.removeRowSelectionInterval(x, x); 
	b=JOptionPane.showConfirmDialog(null, "Do you want to delete this movie from the box office","row deleting",JOptionPane.YES_NO_OPTION,1);
		
}

	private void tablefilmMouseMouseClicked(MouseEvent event) {
	}

	


}

package src.presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;
import org.dyno.visual.swing.layouts.Trailing;


//VS4E -- DO NOT REMOVE THIS LINE!
public class GestionAdmin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField jTextField0;
	private JLabel jLabel0;
	private JTextField jTextField3;
	private JLabel jLabel1;
	private JTextField jTextField4;
	private JButton jButton0;
	private JButton jButton2;
	private JPanel jPanel0;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JPasswordField jPasswordField0;
	private JTextField jTextField2;
	private JPasswordField jPasswordField2;
	private JButton jButton1;
	private JTextField jTextField1;
	private JPanel jPanel1;
	private JButton jButton4;
	private JButton jButton3;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	public GestionAdmin() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getJTextField0(), new Constraints(new Leading(224, -127, 10, 10), new Leading(64, 12, 12)));
		add(getJPanel0(), new Constraints(new Leading(278, 211, 10, 10), new Leading(43, 159, 10, 10)));
		add(getJPanel1(), new Constraints(new Leading(18, 213, 10, 10), new Leading(39, 244, 10, 10)));
		add(getJButton4(), new Constraints(new Leading(16, 87, 10, 10), new Leading(9, 22, 10, 10)));
		add(getJButton3(), new Constraints(new Leading(398, 89, 10, 10), new Leading(9, 22, 12, 12)));
		setSize(534, 303);
	}

	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setText("Log Out");
			jButton3.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton3ActionActionPerformed(event);
				}
			});
		}
		return jButton3;
	}

	private JButton getJButton4() {
		if (jButton4 == null) {
			jButton4 = new JButton();
			jButton4.setText("Back");
			jButton4.addMouseListener(new MouseAdapter() {
	
			);
		}
		return jButton4;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Update Account Informations", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font(
					"Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel1.setLayout(new GroupLayout());
			jPanel1.add(getJLabel2(), new Constraints(new Leading(5, 10, 10), new Leading(15, 10, 10)));
			jPanel1.add(getJLabel3(), new Constraints(new Leading(5, 12, 12), new Leading(45, 10, 10)));
			jPanel1.add(getJLabel4(), new Constraints(new Leading(5, 12, 12), new Leading(91, 10, 10)));
			jPanel1.add(getJLabel5(), new Constraints(new Leading(5, 12, 12), new Leading(125, 12, 12)));
			jPanel1.add(getJPasswordField0(), new Constraints(new Leading(99, 87, 10, 10), new Leading(44, 18, 78, 78)));
			jPanel1.add(getJTextField2(), new Constraints(new Leading(99, 86, 12, 12), new Leading(89, 42, 42)));
			jPanel1.add(getJPasswordField2(), new Constraints(new Leading(99, 86, 12, 12), new Leading(124, 18, 12, 12)));
			jPanel1.add(getJButton1(), new Constraints(new Leading(2, 10, 10), new Trailing(12, 153, 153)));
			jPanel1.add(getJTextField1(), new Constraints(new Leading(99, 86, 12, 12), new Leading(15, 17, 12, 12)));
		}
		return jPanel1;
	}

	private JTextField getJTextField1() {
		if (jTextField1 == null) {
			jTextField1 = new JTextField();
		}
		return jTextField1;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Confirm");
		}
		return jButton1;
	}

	private JPasswordField getJPasswordField2() {
		if (jPasswordField2 == null) {
			jPasswordField2 = new JPasswordField();
			jPasswordField2.setEchoChar('�');
		}
		return jPasswordField2;
	}

	private JTextField getJTextField2() {
		if (jTextField2 == null) {
			jTextField2 = new JTextField();
		}
		return jTextField2;
	}

	private JPasswordField getJPasswordField0() {
		if (jPasswordField0 == null) {
			jPasswordField0 = new JPasswordField();
			jPasswordField0.setEchoChar('�');
		}
		return jPasswordField0;
	}

	private JLabel getJLabel5() {
		if (jLabel5 == null) {
			jLabel5 = new JLabel();
			jLabel5.setText("New Password");
		}
		return jLabel5;
	}

	private JLabel getJLabel4() {
		if (jLabel4 == null) {
			jLabel4 = new JLabel();
			jLabel4.setText("New Login");
		}
		return jLabel4;
	}

	private JLabel getJLabel3() {
		if (jLabel3 == null) {
			jLabel3 = new JLabel();
			jLabel3.setText("Old Password");
		}
		return jLabel3;
	}

	private JLabel getJLabel2() {
		if (jLabel2 == null) {
			jLabel2 = new JLabel();
			jLabel2.setText("Old Login");
		}
		return jLabel2;
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setBorder(BorderFactory.createTitledBorder(null, "Add/ Remove Administrator", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font(
					"Dialog", Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJLabel0(), new Constraints(new Leading(9, 12, 12), new Leading(30, 10, 10)));
			jPanel0.add(getJTextField3(), new Constraints(new Leading(80, 96, 10, 10), new Leading(30, 12, 12)));
			jPanel0.add(getJLabel1(), new Constraints(new Leading(6, 10, 10), new Leading(72, 10, 10)));
			jPanel0.add(getJTextField4(), new Constraints(new Leading(80, 96, 12, 12), new Leading(68, 12, 12)));
			jPanel0.add(getJButton0(), new Constraints(new Leading(3, 10, 10), new Leading(106, 12, 12)));
			jPanel0.add(getJButton2(), new Constraints(new Leading(80, 69, 10, 10), new Leading(106, 12, 12)));
		}
		return jPanel0;
	}

	private JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new JButton();
			jButton2.setText("Add");
		}
		return jButton2;
	}

	private JButton getJButton0() {
		if (jButton0 == null) {
			jButton0 = new JButton();
			jButton0.setText("Delete");
		}
		return jButton0;
	}

	private JTextField getJTextField4() {
		if (jTextField4 == null) {
			jTextField4 = new JTextField();
		}
		return jTextField4;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("Password");
		}
		return jLabel1;
	}

	private JTextField getJTextField3() {
		if (jTextField3 == null) {
			jTextField3 = new JTextField();
		}
		return jTextField3;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Login");
		}
		return jLabel0;
	}

	private JTextField getJTextField0() {
		if (jTextField0 == null) {
			jTextField0 = new JTextField();
		}
		return jTextField0;
	}

	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			if (lnfClassname == null)
				lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				GestionAdmin frame = new GestionAdmin();
				frame.setDefaultCloseOperation(GestionAdmin.EXIT_ON_CLOSE);
				frame.setTitle("GestionAdmin");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private void jButton3ActionActionPerformed(ActionEvent event) {
		Auth au =new Auth();
		this.setVisible(false);
		au.setVisible(true);
	}

	private void jButton4MouseMouseClicked(MouseEvent event) {
		this.dispose(); 
		Acceuil a = new Acceuil(); 
		a.setVisible(true); 
		
	}

}

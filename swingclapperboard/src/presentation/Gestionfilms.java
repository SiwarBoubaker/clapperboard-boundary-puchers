package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;


//VS4E -- DO NOT REMOVE THIS LINE!
public class Gestionfilms extends JFrame {

	private static final long serialVersionUID = 1L;
	private JButton jButton4;
	private JLabel jLabel1;
	private JButton jButton0;
	private JTextField jTextField0;
	private JTextField jTextField1;
	private JTextField jTextField3;
	private JTextField jTextField4;
	private JTextField jTextField2;
	private JLabel jLabel0;
	private JLabel jLabel2;
	private JLabel jLabel4;
	private JLabel jLabel3;
	private JPanel jPanel1;
	private JTable jTable0;
	private JScrollPane jScrollPane0;
	private JButton jButton1;
	private JButton jButton2;
	private JPanel jPanel2;
	private JButton jButton3;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private JLabel jLabel8;
	private JLabel jLabel9;
	private JLabel jLabel12;
	private JLabel jLabel10;
	private JLabel jLabel11;
	private JTextField jTextField5;
	private JTextField jTextField8;
	private JTextField jTextField9;
	private JTextField jTextField10;
	private JTextField jTextField11;
	private JTextField jTextField12;
	private JTextField jTextField7;
	private JTextField jTextField6;
	private JPanel jPanel3;
	private JPanel jPanel0;
	private JButton jButton5;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	public Gestionfilms() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getJPanel0(), new Constraints(new Leading(109, 645, 10, 10), new Leading(44, 10, 10)));
		add(getJButton5(), new Constraints(new Leading(672, 80, 10, 10), new Leading(5, 24, 10, 10)));
		add(getJButton4(), new Constraints(new Leading(46, 75, 10, 10), new Leading(12, 12, 12)));
		setSize(789, 559);
	}

	private JButton getJButton5() {
		if (jButton5 == null) {
			jButton5 = new JButton();
			jButton5.setText("Log Out");
			jButton5.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton5ActionActionPerformed(event);
				}
			});
		}
		return jButton5;
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setBorder(BorderFactory.createTitledBorder(null, "Movies Space", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJPanel2(), new Constraints(new Leading(18, 321, 10, 10), new Leading(27, 437, 10, 10)));
			jPanel0.add(getJPanel3(), new Constraints(new Leading(357, 261, 12, 12), new Leading(29, 384, 10, 10)));
		}
		return jPanel0;
	}

	private JPanel getJPanel3() {
		if (jPanel3 == null) {
			jPanel3 = new JPanel();
			jPanel3.setBorder(BorderFactory.createTitledBorder(null, "New Movies", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel3.setLayout(new GroupLayout());
			jPanel3.add(getJButton3(), new Constraints(new Leading(6, 10, 10), new Leading(329, 23, 10, 10)));
			jPanel3.add(getJLabel5(), new Constraints(new Leading(10, 10, 10), new Leading(22, 10, 10)));
			jPanel3.add(getJLabel6(), new Constraints(new Leading(10, 12, 12), new Leading(59, 10, 10)));
			jPanel3.add(getJLabel7(), new Constraints(new Leading(12, 12, 12), new Leading(93, 12, 12)));
			jPanel3.add(getJLabel8(), new Constraints(new Leading(12, 12, 12), new Leading(127, 12, 12)));
			jPanel3.add(getJLabel9(), new Constraints(new Leading(16, 12, 12), new Leading(261, 10, 10)));
			jPanel3.add(getJLabel12(), new Constraints(new Leading(12, 12, 12), new Leading(161, 12, 12)));
			jPanel3.add(getJLabel10(), new Constraints(new Leading(12, 12, 12), new Leading(195, 12, 12)));
			jPanel3.add(getJLabel11(), new Constraints(new Leading(12, 12, 12), new Leading(225, 18, 12, 12)));
			jPanel3.add(getJTextField5(), new Constraints(new Leading(84, 77, 10, 10), new Leading(18, 12, 12)));
			jPanel3.add(getJTextField8(), new Constraints(new Leading(82, 80, 12, 12), new Leading(125, 12, 12)));
			jPanel3.add(getJTextField9(), new Constraints(new Leading(82, 80, 12, 12), new Leading(157, 12, 12)));
			jPanel3.add(getJTextField10(), new Constraints(new Leading(80, 80, 12, 12), new Leading(193, 12, 12)));
			jPanel3.add(getJTextField11(), new Constraints(new Leading(80, 80, 12, 12), new Leading(225, 12, 12)));
			jPanel3.add(getJTextField12(), new Constraints(new Leading(80, 80, 12, 12), new Leading(257, 12, 12)));
			jPanel3.add(getJTextField7(), new Constraints(new Leading(84, 76, 12, 12), new Leading(91, 12, 12)));
			jPanel3.add(getJTextField6(), new Constraints(new Leading(83, 77, 12, 12), new Leading(56, 12, 12)));
		}
		return jPanel3;
	}

	private JTextField getJTextField6() {
		if (jTextField6 == null) {
			jTextField6 = new JTextField();
		}
		return jTextField6;
	}

	private JTextField getJTextField7() {
		if (jTextField7 == null) {
			jTextField7 = new JTextField();
		}
		return jTextField7;
	}

	private JTextField getJTextField12() {
		if (jTextField12 == null) {
			jTextField12 = new JTextField();
		}
		return jTextField12;
	}

	private JTextField getJTextField11() {
		if (jTextField11 == null) {
			jTextField11 = new JTextField();
		}
		return jTextField11;
	}

	private JTextField getJTextField10() {
		if (jTextField10 == null) {
			jTextField10 = new JTextField();
		}
		return jTextField10;
	}

	private JTextField getJTextField9() {
		if (jTextField9 == null) {
			jTextField9 = new JTextField();
		}
		return jTextField9;
	}

	private JTextField getJTextField8() {
		if (jTextField8 == null) {
			jTextField8 = new JTextField();
		}
		return jTextField8;
	}

	private JTextField getJTextField5() {
		if (jTextField5 == null) {
			jTextField5 = new JTextField();
		}
		return jTextField5;
	}

	private JLabel getJLabel11() {
		if (jLabel11 == null) {
			jLabel11 = new JLabel();
			jLabel11.setText("Rank BO");
		}
		return jLabel11;
	}

	private JLabel getJLabel10() {
		if (jLabel10 == null) {
			jLabel10 = new JLabel();
			jLabel10.setText("Premiere");
		}
		return jLabel10;
	}

	private JLabel getJLabel12() {
		if (jLabel12 == null) {
			jLabel12 = new JLabel();
			jLabel12.setText("Type");
		}
		return jLabel12;
	}

	private JLabel getJLabel9() {
		if (jLabel9 == null) {
			jLabel9 = new JLabel();
			jLabel9.setText("Year");
		}
		return jLabel9;
	}

	private JLabel getJLabel8() {
		if (jLabel8 == null) {
			jLabel8 = new JLabel();
			jLabel8.setText("Actors");
		}
		return jLabel8;
	}

	private JLabel getJLabel7() {
		if (jLabel7 == null) {
			jLabel7 = new JLabel();
			jLabel7.setText("Production");
		}
		return jLabel7;
	}

	private JLabel getJLabel6() {
		if (jLabel6 == null) {
			jLabel6 = new JLabel();
			jLabel6.setText("Author");
		}
		return jLabel6;
	}

	private JLabel getJLabel5() {
		if (jLabel5 == null) {
			jLabel5 = new JLabel();
			jLabel5.setText("Title");
		}
		return jLabel5;
	}

	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setText("Add Movie");
		}
		return jButton3;
	}

	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Existing Movies", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel2.setLayout(new GroupLayout());
			jPanel2.add(getJPanel1(), new Constraints(new Leading(10, 10, 10), new Leading(10, 10, 10)));
			jPanel2.add(getJScrollPane0(), new Constraints(new Leading(12, 200, 12, 12), new Leading(232, 150, 10, 10)));
			jPanel2.add(getJButton1(), new Constraints(new Leading(12, 74, 12, 12), new Leading(384, 22, 10, 10)));
			jPanel2.add(getJButton2(), new Constraints(new Leading(98, 12, 12), new Leading(384, 22, 12, 12)));
		}
		return jPanel2;
	}

	private JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new JButton();
			jButton2.setText("Update");
		}
		return jButton2;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Delete");
		}
		return jButton1;
	}

	private JScrollPane getJScrollPane0() {
		if (jScrollPane0 == null) {
			jScrollPane0 = new JScrollPane();
			jScrollPane0.setViewportView(getJTable0());
		}
		return jScrollPane0;
	}

	private JTable getJTable0() {
		if (jTable0 == null) {
			jTable0 = new JTable();
			jTable0.setModel(new DefaultTableModel(new Object[][] { { " ", "null", }, }, new String[] { "Title  ", "Type", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
		}
		return jTable0;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Quick Search", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel1.setLayout(new GroupLayout());
			jPanel1.add(getJLabel1(), new Constraints(new Leading(12, 12, 12), new Leading(31, 10, 10)));
			jPanel1.add(getJButton0(), new Constraints(new Leading(3, 10, 10), new Leading(139, 22, 12, 12)));
			jPanel1.add(getJTextField0(), new Constraints(new Leading(67, 74, 10, 10), new Leading(6, 12, 12)));
			jPanel1.add(getJTextField1(), new Constraints(new Leading(67, 74, 12, 12), new Leading(31, 12, 12)));
			jPanel1.add(getJTextField3(), new Constraints(new Leading(65, 74, 12, 12), new Leading(81, 12, 12)));
			jPanel1.add(getJTextField4(), new Constraints(new Leading(65, 74, 12, 12), new Leading(107, 12, 12)));
			jPanel1.add(getJTextField2(), new Constraints(new Leading(65, 74, 12, 12), new Leading(55, 12, 12)));
			jPanel1.add(getJLabel0(), new Constraints(new Leading(12, 12, 12), new Leading(6, 12, 12)));
			jPanel1.add(getJLabel2(), new Constraints(new Leading(12, 12, 12), new Leading(53, 12, 12)));
			jPanel1.add(getJLabel4(), new Constraints(new Leading(12, 12, 12), new Leading(105, 12, 12)));
			jPanel1.add(getJLabel3(), new Constraints(new Leading(12, 12, 12), new Leading(77, 12, 12)));
		}
		return jPanel1;
	}

	private JLabel getJLabel3() {
		if (jLabel3 == null) {
			jLabel3 = new JLabel();
			jLabel3.setText("Year");
		}
		return jLabel3;
	}

	private JLabel getJLabel4() {
		if (jLabel4 == null) {
			jLabel4 = new JLabel();
			jLabel4.setText("Type");
		}
		return jLabel4;
	}

	private JLabel getJLabel2() {
		if (jLabel2 == null) {
			jLabel2 = new JLabel();
			jLabel2.setText("Actor");
		}
		return jLabel2;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Title");
		}
		return jLabel0;
	}

	private JTextField getJTextField2() {
		if (jTextField2 == null) {
			jTextField2 = new JTextField();
		}
		return jTextField2;
	}

	private JTextField getJTextField4() {
		if (jTextField4 == null) {
			jTextField4 = new JTextField();
		}
		return jTextField4;
	}

	private JTextField getJTextField3() {
		if (jTextField3 == null) {
			jTextField3 = new JTextField();
		}
		return jTextField3;
	}

	private JTextField getJTextField1() {
		if (jTextField1 == null) {
			jTextField1 = new JTextField();
		}
		return jTextField1;
	}

	private JTextField getJTextField0() {
		if (jTextField0 == null) {
			jTextField0 = new JTextField();
		}
		return jTextField0;
	}

	private JButton getJButton0() {
		if (jButton0 == null) {
			jButton0 = new JButton();
			jButton0.setText("Search");
		}
		return jButton0;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("Author");
		}
		return jLabel1;
	}

	private JButton getJButton4() {
		if (jButton4 == null) {
			jButton4 = new JButton();
			jButton4.setText("Back");
			jButton4.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton4MouseMouseClicked(event);
				}
			});
		}
		return jButton4;
	}

	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			if (lnfClassname == null)
				lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Gestionfilms frame = new Gestionfilms();
				frame.setDefaultCloseOperation(Gestionfilms.EXIT_ON_CLOSE);
				frame.setTitle("gestionfilms");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private void jButton5ActionActionPerformed(ActionEvent event) {
		Auth au =new Auth();
	this.setVisible(false);
	au.setVisible(true);
		
	}

	private void jButton4MouseMouseClicked(MouseEvent event) {
		this.dispose(); 
		Acceuil a = new Acceuil(); 
		a.setVisible(true); 
		
	}

}

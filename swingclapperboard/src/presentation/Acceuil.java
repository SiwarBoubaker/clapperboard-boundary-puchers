package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;


//VS4E -- DO NOT REMOVE THIS LINE!
public class Acceuil extends JFrame {

	private static final long serialVersionUID = 1L;
	private JButton jButton4;
	private JButton jButton3;
	private JPanel jPanel0;
	private JButton jButton1;
	private JButton jButton0;
	private JPanel jPanel1;
	private JButton jButton5;
	private JButton jButton2;
	private JButton jButton6;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	public Acceuil() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getJButton5(), new Constraints(new Leading(15, 77, 10, 10), new Leading(8, 23, 12, 12)));
		add(getJButton2(), new Constraints(new Leading(338, 10, 10), new Leading(7, 24, 12, 12)));
		add(getJPanel0(), new Constraints(new Leading(227, 189, 12, 12), new Leading(61, 153, 10, 10)));
		add(getJPanel1(), new Constraints(new Leading(12, 179, 12, 12), new Leading(60, 202, 10, 10)));
		setSize(436, 285);
	}

	private JButton getJButton6() {
		if (jButton6 == null) {
			jButton6 = new JButton();
			jButton6.setText("Manage BoxOffice");
			jButton6.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton6MouseMouseClicked(event);
				}
			});
		}
		return jButton6;
	}

	private JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new JButton();
			jButton2.setText("Log Out");
		}
		return jButton2;
	}

	private JButton getJButton5() {
		if (jButton5 == null) {
			jButton5 = new JButton();
			jButton5.setText("Back");
			jButton5.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton5MouseMouseClicked(event);
				}
			});
		}
		return jButton5;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBorder(BorderFactory.createTitledBorder(null, "News Management", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel1.setLayout(new GroupLayout());
			jPanel1.add(getJButton0(), new Constraints(new Leading(10, 150, 12, 12), new Leading(22, 10, 10)));
			jPanel1.add(getJButton1(), new Constraints(new Leading(10, 150, 10, 10), new Leading(77, 10, 10)));
			jPanel1.add(getJButton6(), new Constraints(new Leading(12, 146, 12, 12), new Leading(131, 30, 10, 10)));
		}
		return jPanel1;
	}

	private JButton getJButton0() {
		if (jButton0 == null) {
			jButton0 = new JButton();
			jButton0.setText("Manage Movies");
			jButton0.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton0ActionActionPerformed(event);
				}
			});
		}
		return jButton0;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Manage Quizz");
			jButton1.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton1ActionActionPerformed(event);
				}
			});
		}
		return jButton1;
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setBorder(BorderFactory.createTitledBorder(null, "Accounts Management", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Dialog",
					Font.BOLD, 12), new Color(51, 51, 51)));
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJButton4(), new Constraints(new Leading(15, 12, 12), new Leading(76, 10, 10)));
			jPanel0.add(getJButton3(), new Constraints(new Leading(17, 155, 12, 12), new Leading(22, 10, 10)));
		}
		return jPanel0;
	}

	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setText("Manage Users");
			jButton3.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton3ActionActionPerformed(event);
				}
			});
		}
		return jButton3;
	}

	private JButton getJButton4() {
		if (jButton4 == null) {
			jButton4 = new JButton();
			jButton4.setText("Administrators Space");
			jButton4.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent event) {
					jButton4ActionActionPerformed(event);
				}
			});
		}
		return jButton4;
	}

	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			if (lnfClassname == null)
				lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Acceuil frame = new Acceuil();
				frame.setDefaultCloseOperation(Acceuil.EXIT_ON_CLOSE);
				frame.setTitle("Acceuil");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private void jButton2ActionActionPerformed(ActionEvent event) {
		Auth b=new Auth();
		b.setVisible(true);
		this.setVisible(false);
	}

	private void jButton3ActionActionPerformed(ActionEvent event) {
		Accounts ac =new Accounts();
		this.setVisible(false);
		ac.setVisible(true);
	}

	private void jButton4ActionActionPerformed(ActionEvent event) {
		GestionAdmin ges =new GestionAdmin();
		this.setVisible(false);
		ges.setVisible(true);
	}

	private void jButton0ActionActionPerformed(ActionEvent event) {
		Gestionfilms gf= new Gestionfilms();
		this.setVisible(false);
		gf.setVisible(true);
		
	}

	private void jButton1ActionActionPerformed(ActionEvent event) {
		GestionQuizz gq =new GestionQuizz();
		this.setVisible(false);
		gq.setVisible(true);
	}

	private void jButton5MouseMouseClicked(MouseEvent event) {
		this.dispose(); 
		Auth a= new Auth(); 
		a.setVisible(true); 
	}

	private void jButton6MouseMouseClicked(MouseEvent event) {
		GestionBoxOffice a = new GestionBoxOffice(); 
		this.dispose(); 
		a.setVisible(true); 
		
	}

}

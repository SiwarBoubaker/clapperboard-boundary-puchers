package presentation;

import java.awt.event.MouseAdapter;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;
import org.dyno.visual.swing.layouts.Trailing;


//VS4E -- DO NOT REMOVE THIS LINE!
public class Accounts extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jPanel1;
	private JPanel jPanel0;
	private JTable jTable2;
	private JScrollPane jScrollPane2;
	private JLabel jLabel0;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JTextField jTextField1;
	private JTextField jTextField0;
	private JComboBox jComboBox0;
	private JButton jButton1;
	private JButton jButton0;
	private JButton jButton2;
	private JPanel jPanel3;
	private JTable jTable1;
	private JScrollPane jScrollPane1;
	private JTable jTable0;
	private JScrollPane jScrollPane0;
	private JButton jButton3;
	private JButton jButton4;
	private JPanel jPanel2;
	private JButton jButton6;
	private JButton jButton5;
	private static final String PREFERRED_LOOK_AND_FEEL = "javax.swing.plaf.metal.MetalLookAndFeel";
	public Accounts() {
		initComponents();
	}

	private void initComponents() {
		setLayout(new GroupLayout());
		add(getJButton6(), new Constraints(new Leading(26, 82, 10, 10), new Leading(8, 10, 10)));
		add(getJButton5(), new Constraints(new Leading(647, 90, 10, 10), new Leading(10, 12, 12)));
		add(getJPanel3(), new Constraints(new Leading(454, 286, 10, 10), new Leading(48, 497, 10, 10)));
		add(getJPanel2(), new Constraints(new Leading(26, 406, 10, 10), new Leading(42, 503, 12, 12)));
		setSize(771, 574);
	}

	private JButton getJButton5() {
		if (jButton5 == null) {
			jButton5 = new JButton();
			jButton5.setText("Log Out");
		}
		return jButton5;
	}

	private JButton getJButton6() {
		if (jButton6 == null) {
			jButton6 = new JButton();
			jButton6.setText("Back");
			jButton6.addMouseListener(new MouseAdapter() {
	
				public void mouseClicked(MouseEvent event) {
					jButton6MouseMouseClicked(event);
				}
			});
		}
		return jButton6;
	}

	private JPanel getJPanel2() {
		if (jPanel2 == null) {
			jPanel2 = new JPanel();
			jPanel2.setBorder(BorderFactory.createTitledBorder(null, "Users", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, null));
			jPanel2.setLayout(new GroupLayout());
			jPanel2.add(getJPanel1(), new Constraints(new Leading(15, 249, 10, 10), new Leading(224, 202, 10, 10)));
			jPanel2.add(getJPanel0(), new Constraints(new Leading(15, 248, 12, 12), new Leading(14, 201, 10, 10)));
			jPanel2.add(getJButton3(), new Constraints(new Leading(51, 79, 10, 10), new Leading(438, 24, 12, 12)));
			jPanel2.add(getJButton4(), new Constraints(new Leading(139, 84, 10, 10), new Leading(438, 24, 12, 12)));
		}
		return jPanel2;
	}

	private JButton getJButton4() {
		if (jButton4 == null) {
			jButton4 = new JButton();
			jButton4.setText("Refuse");
		}
		return jButton4;
	}

	private JButton getJButton3() {
		if (jButton3 == null) {
			jButton3 = new JButton();
			jButton3.setText("Accept");
		}
		return jButton3;
	}

	private JScrollPane getJScrollPane0() {
		if (jScrollPane0 == null) {
			jScrollPane0 = new JScrollPane();
			jScrollPane0.setViewportView(getJTable0());
		}
		return jScrollPane0;
	}

	private JTable getJTable0() {
		if (jTable0 == null) {
			jTable0 = new JTable();
			jTable0.setModel(new DefaultTableModel(new Object[][] { { "0x0", "0x1", }, { "1x0", "1x1", }, }, new String[] { "Title 0", "Title 1", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
		}
		return jTable0;
	}

	private JScrollPane getJScrollPane1() {
		if (jScrollPane1 == null) {
			jScrollPane1 = new JScrollPane();
			jScrollPane1.setViewportView(getJTable1());
		}
		return jScrollPane1;
	}

	private JTable getJTable1() {
		if (jTable1 == null) {
			jTable1 = new JTable();
			jTable1.setModel(new DefaultTableModel(new Object[][] { { "0x0", "0x1", }, { "1x0", "1x1", }, }, new String[] { "Title 0", "Title 1", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
		}
		return jTable1;
	}

	private JPanel getJPanel3() {
		if (jPanel3 == null) {
			jPanel3 = new JPanel();
			jPanel3.setBorder(BorderFactory.createTitledBorder(null, "Quick Search", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, null));
			jPanel3.setLayout(new GroupLayout());
			jPanel3.add(getJScrollPane2(), new Constraints(new Leading(13, 200, 10, 10), new Leading(147, 150, 10, 10)));
			jPanel3.add(getJLabel0(), new Constraints(new Leading(16, 10, 10), new Leading(17, 12, 12)));
			jPanel3.add(getJLabel1(), new Constraints(new Leading(17, 12, 12), new Leading(43, 12, 12)));
			jPanel3.add(getJLabel2(), new Constraints(new Leading(19, 10, 10), new Leading(71, 12, 12)));
			jPanel3.add(getJTextField1(), new Constraints(new Leading(67, 84, 12, 12), new Leading(39, 12, 12)));
			jPanel3.add(getJTextField0(), new Constraints(new Leading(67, 84, 12, 12), new Leading(12, 12, 12)));
			jPanel3.add(getJComboBox0(), new Constraints(new Leading(67, 60, 12, 12), new Leading(69, 20, 12, 12)));
			jPanel3.add(getJButton1(), new Constraints(new Leading(89, 76, 12, 12), new Leading(324, 12, 12)));
			jPanel3.add(getJButton0(), new Constraints(new Leading(12, 71, 12, 12), new Leading(324, 10, 10)));
			jPanel3.add(getJButton2(), new Constraints(new Leading(171, 77, 10, 10), new Leading(324, 12, 12)));
		}
		return jPanel3;
	}

	private JButton getJButton2() {
		if (jButton2 == null) {
			jButton2 = new JButton();
			jButton2.setText("Notify");
		}
		return jButton2;
	}

	private JButton getJButton0() {
		if (jButton0 == null) {
			jButton0 = new JButton();
			jButton0.setText("Delete");
		}
		return jButton0;
	}

	private JButton getJButton1() {
		if (jButton1 == null) {
			jButton1 = new JButton();
			jButton1.setText("Warn");
		}
		return jButton1;
	}

	private JComboBox getJComboBox0() {
		if (jComboBox0 == null) {
			jComboBox0 = new JComboBox();
			jComboBox0.setModel(new DefaultComboBoxModel(new Object[] {}));
			jComboBox0.setDoubleBuffered(false);
			jComboBox0.setBorder(null);
		}
		return jComboBox0;
	}

	private JTextField getJTextField0() {
		if (jTextField0 == null) {
			jTextField0 = new JTextField();
		}
		return jTextField0;
	}

	private JTextField getJTextField1() {
		if (jTextField1 == null) {
			jTextField1 = new JTextField();
		}
		return jTextField1;
	}

	private JLabel getJLabel2() {
		if (jLabel2 == null) {
			jLabel2 = new JLabel();
			jLabel2.setText("Age");
		}
		return jLabel2;
	}

	private JLabel getJLabel1() {
		if (jLabel1 == null) {
			jLabel1 = new JLabel();
			jLabel1.setText("Login");
		}
		return jLabel1;
	}

	private JLabel getJLabel0() {
		if (jLabel0 == null) {
			jLabel0 = new JLabel();
			jLabel0.setText("Name");
		}
		return jLabel0;
	}

	private JScrollPane getJScrollPane2() {
		if (jScrollPane2 == null) {
			jScrollPane2 = new JScrollPane();
			jScrollPane2.setViewportView(getJTable2());
		}
		return jScrollPane2;
	}

	private JTable getJTable2() {
		if (jTable2 == null) {
			jTable2 = new JTable();
			jTable2.setModel(new DefaultTableModel(new Object[][] { { "0x0", "0x1", }, { "1x0", "1x1", }, }, new String[] { "Title 0", "Title 1", }) {
				private static final long serialVersionUID = 1L;
				Class<?>[] types = new Class<?>[] { Object.class, Object.class, };
	
				public Class<?> getColumnClass(int columnIndex) {
					return types[columnIndex];
				}
			});
		}
		return jTable2;
	}

	private JPanel getJPanel0() {
		if (jPanel0 == null) {
			jPanel0 = new JPanel();
			jPanel0.setBorder(BorderFactory.createTitledBorder(null, "Lambda User", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, null));
			jPanel0.setLayout(new GroupLayout());
			jPanel0.add(getJScrollPane0(), new Constraints(new Leading(12, 200, 12, 12), new Leading(5, 150, 10, 10)));
		}
		return jPanel0;
	}

	private JPanel getJPanel1() {
		if (jPanel1 == null) {
			jPanel1 = new JPanel();
			jPanel1.setBorder(BorderFactory.createTitledBorder(null, "Cineasts", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, null, null));
			jPanel1.setLayout(new GroupLayout());
			jPanel1.add(getJScrollPane1(), new Constraints(new Leading(26, 200, 10, 10), new Trailing(12, 150, 12, 12)));
		}
		return jPanel1;
	}

	private static void installLnF() {
		try {
			String lnfClassname = PREFERRED_LOOK_AND_FEEL;
			if (lnfClassname == null)
				lnfClassname = UIManager.getCrossPlatformLookAndFeelClassName();
			UIManager.setLookAndFeel(lnfClassname);
		} catch (Exception e) {
			System.err.println("Cannot install " + PREFERRED_LOOK_AND_FEEL
					+ " on this platform:" + e.getMessage());
		}
	}

	/**
	 * Main entry of the class.
	 * Note: This class is only created so that you can easily preview the result at runtime.
	 * It is not expected to be managed by the designer.
	 * You can modify it as you like.
	 */
	public static void main(String[] args) {
		installLnF();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Accounts frame = new Accounts();
				frame.setDefaultCloseOperation(Accounts.EXIT_ON_CLOSE);
				frame.setTitle("Actualite_prim");
				frame.getContentPane().setPreferredSize(frame.getSize());
				frame.pack();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
	}

	private void jButton6MouseMouseClicked(MouseEvent event) {
		this.dispose(); 
		Acceuil a= new Acceuil(); 
		a.setVisible(true); 
	}

}

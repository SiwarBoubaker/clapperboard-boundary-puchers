package services.interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Trailer;

@Local
public interface TrailerDAOLocal {

    public void create(Trailer b);
	
	public Trailer retrieve(int id);
	
	public void update(Trailer b);
	
	public void delete(Trailer b);
	
	public List<Trailer> retrieveAll();

}

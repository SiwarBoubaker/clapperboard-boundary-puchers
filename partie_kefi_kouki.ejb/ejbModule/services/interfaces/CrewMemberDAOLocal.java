package services.interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.CrewMember;

@Local
public interface CrewMemberDAOLocal {

    public void create(CrewMember b);
	
	public CrewMember retrieve(int id);
	
	public void update(CrewMember b);
	
	public void delete(CrewMember b);
	
	public List<CrewMember> retrieveAll();

}

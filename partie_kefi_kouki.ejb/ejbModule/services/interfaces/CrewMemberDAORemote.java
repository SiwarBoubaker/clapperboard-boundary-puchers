package services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import entities.CrewMember;

@Remote
public interface CrewMemberDAORemote {

    public void create(CrewMember b);
	
	public CrewMember retrieve(int id);
	
	public void update(CrewMember b);
	
	public void delete(CrewMember b);
	
	public List<CrewMember> retrieveAll();

}

package services.interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Movie;

@Local
public interface MovieDAOLocal {

    public void create(Movie b);
	
	public Movie retrieve(int id);
	
	public void update(Movie b);
	
	public void delete(Movie b);
	
	public List<Movie> retrieveAll();

}

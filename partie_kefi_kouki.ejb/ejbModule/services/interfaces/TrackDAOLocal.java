package services.interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Track;

@Local
public interface TrackDAOLocal {

    public void create(Track b);
	
	public Track retrieve(int id);
	
	public void update(Track b);
	
	public void delete(Track b);
	
	public List<Track> retrieveAll();

}

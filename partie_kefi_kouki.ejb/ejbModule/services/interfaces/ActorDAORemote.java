package services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import entities.Actor;

@Remote
public interface ActorDAORemote {

    public void create(Actor b);
	
	public Actor retrieve(int id);
	
	public void update(Actor b);
	
	public void delete(Actor b);
	
	public List<Actor> retrieveAll();

}

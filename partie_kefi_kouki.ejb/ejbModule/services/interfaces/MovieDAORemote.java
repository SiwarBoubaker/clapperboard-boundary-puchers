package services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import entities.Movie;

@Remote
public interface MovieDAORemote {

    public void create(Movie b);
	
	public Movie retrieve(int id);
	
	public void update(Movie b);
	
	public void delete(Movie b);
	
	public List<Movie> retrieveAll();

}

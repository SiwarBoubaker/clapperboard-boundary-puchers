package services.interfaces;

import java.util.List;

import javax.ejb.Remote;

import entities.Trailer;

@Remote
public interface TrailerDAORemote {

    public void create(Trailer b);
	
	public Trailer retrieve(int id);
	
	public void update(Trailer b);
	
	public void delete(Trailer b);
	
	public List<Trailer> retrieveAll();

}

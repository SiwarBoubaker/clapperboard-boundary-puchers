package services.interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Actor;





@Local
public interface ActorDAOLocal {

    public void create(Actor b);
	
	public Actor retrieve(int id);
	
	public void update(Actor b);
	
	public void delete(Actor b);
	
	public List<Actor> retrieveAll();

}

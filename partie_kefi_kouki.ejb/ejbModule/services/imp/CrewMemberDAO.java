package services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import services.interfaces.CrewMemberDAOLocal;
import services.interfaces.CrewMemberDAORemote;
import entities.CrewMember;



/**
 * Session Bean implementation class CrewMemberDAO
 */
@Stateless
@LocalBean
public class CrewMemberDAO implements CrewMemberDAORemote, CrewMemberDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public CrewMemberDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(CrewMember b) {
		em.persist(b);
		
	}

	@Override
	public CrewMember retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(CrewMember.class, id);
	}

	@Override
	public void update(CrewMember b) {
		em.merge(b);
		
	}

	@Override
	public void delete(CrewMember b) {
		CrewMember x;
		x=em.find(CrewMember.class, b.getParticipantId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<CrewMember> retrieveAll() {
		TypedQuery<CrewMember> query = em.createQuery("select a from CrewMember a ",CrewMember.class);
		return query.getResultList();
	}

}

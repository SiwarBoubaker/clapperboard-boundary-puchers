package services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import services.interfaces.TrackDAOLocal;
import services.interfaces.TrackDAORemote;
import entities.Track;



/**
 * Session Bean implementation class TrackDAO
 */
@Stateless
@LocalBean
public class TrackDAO implements TrackDAORemote, TrackDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public TrackDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Track b) {
		em.persist(b);
		
	}

	@Override
	public Track retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Track.class, id);
	}

	@Override
	public void update(Track b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Track b) {
		Track x;
		x=em.find(Track.class, b.getTrackId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Track> retrieveAll() {
		TypedQuery<Track> query = em.createQuery("select a from Track a ",Track.class);
		return query.getResultList();
	}

}

package services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import services.interfaces.MovieDAOLocal;
import services.interfaces.MovieDAORemote;
import entities.Movie;



/**
 * Session Bean implementation class MovieDAO
 */
@Stateless
@LocalBean
public class MovieDAO implements MovieDAORemote, MovieDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public MovieDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Movie b) {
		em.persist(b);
		
	}

	@Override
	public Movie retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Movie.class, id);
	}

	@Override
	public void update(Movie b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Movie b) {
		Movie x;
		x=em.find(Movie.class, b.getMovieId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Movie> retrieveAll() {
		TypedQuery<Movie> query = em.createQuery("select a from Movie a ",Movie.class);
		return query.getResultList();
	}

}

package services.imp;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import services.interfaces.TrailerDAOLocal;
import services.interfaces.TrailerDAORemote;
import entities.Trailer;



/**
 * Session Bean implementation class TrailerDAO
 */
@Stateless
@LocalBean
public class TrailerDAO implements TrailerDAORemote, TrailerDAOLocal {

	@PersistenceContext
    EntityManager em;
	
    public TrailerDAO() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void create(Trailer b) {
		em.persist(b);
		
	}

	@Override
	public Trailer retrieve(int id) {
		// TODO Auto-generated method stub
		return em.find(Trailer.class, id);
	}

	@Override
	public void update(Trailer b) {
		em.merge(b);
		
	}

	@Override
	public void delete(Trailer b) {
		Trailer x;
		x=em.find(Trailer.class, b.getTrailerId());
		em.remove(x);
		em.flush();
		
	}

	@Override
	public List<Trailer> retrieveAll() {
		TypedQuery<Trailer> query = em.createQuery("select a from Trailer a ",Trailer.class);
		return query.getResultList();
	}

}

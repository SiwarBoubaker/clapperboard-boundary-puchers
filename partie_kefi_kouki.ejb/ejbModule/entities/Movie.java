package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;




@Entity

public class Movie implements Serializable {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int movieId;
	private String name;
	private Date releaseDate;
	private String duration;
	private String synopsis;
	private String poster;
	private String genre;
	//@OneToOne
	//private Premiere premiere;
	//@OneToMany(mappedBy="movie")
//	private Collection<Fact> facts = new ArrayList<Fact>();
//	@ManyToMany
	//private Collection<Theater> inTheaters = new ArrayList<Theater>();
	@OneToMany(mappedBy="movie")
	private Collection<Track> soundTrack=new ArrayList<Track>();
	@OneToMany(mappedBy="movie")
	private Collection<Trailer> trailers=new ArrayList<Trailer>();
	@ManyToMany
	private Collection<Participant> participants = new ArrayList<Participant>();
	
	
	private static final long serialVersionUID = 1L;

	public Movie() {
		super();
	}   
	 
	
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
   
	   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}   
	
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getSynopsis() {
		return this.synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/*public Premiere getPremiere() {
		return premiere;
	}

	public void setPremiere(Premiere premiere) {
		this.premiere = premiere;
	}

	public Collection<Fact> getFacts() {
		return facts;
	}

	public void setFacts(Collection<Fact> facts) {
		this.facts = facts;
	}

	public Collection<Theater> getInTheaters() {
		return inTheaters;
	}

	public void setInTheaters(Collection<Theater> inTheaters) {
		this.inTheaters = inTheaters;
	}*/

	public Collection<Track> getSoundTrack() {
		return soundTrack;
	}

	public void setSoundTrack(Collection<Track> soundTrack) {
		this.soundTrack = soundTrack;
	}

	public Collection<Trailer> getTrailers() {
		return trailers;
	}

	public void setTrailers(Collection<Trailer> trailers) {
		this.trailers = trailers;
	}

	public Collection<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(Collection<Participant> participants) {
		this.participants = participants;
	}
	
	
}

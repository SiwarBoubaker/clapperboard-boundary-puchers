package entities;

import java.io.Serializable;

import javax.persistence.Entity;


/**
 * Entity implementation class for Entity: CrewMember
 *
 */
@Entity

public class CrewMember extends Participant implements Serializable {

	private String job;
	private static final long serialVersionUID = 1L;


	public CrewMember() {
		super();
	}
	
	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	
   
}

package entities;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * Entity implementation class for Entity: Actor
 *
 */
@Entity

public class Actor extends Participant implements Serializable {

	private String nationality;
	private String awards;
	private static final long serialVersionUID = 1L;
	
	public Actor() {
		super();
	}

	
	
	
	public String getNationality() {
		return nationality;
	}


	public void setNationality(String nationality) {
		this.nationality = nationality;
	}


	
	

	

	public String getAwards() {
		return awards;
	}


	public void setAwards(String awards) {
		this.awards = awards;
	}
	
	
   
}

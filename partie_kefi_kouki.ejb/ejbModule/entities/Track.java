package entities;


import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Track
 *
 */

@Entity
public class Track  {

	@Id
	private int trackId;
	private String title;
	private String composer;
	private String duration;
	@ManyToOne
	private Movie movie;
	
	
	public Track() {
		super();
	}   
	   
	 
	
	public int getTrackId() {
		return trackId;
	}



	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}



	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getComposer() {
		return this.composer;
	}

	public void setComposer(String composer) {
		this.composer = composer;
	}   
	public String getDuration() {
		return this.duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
   
}

package entities;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Participant
 *
 */
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE) 
public abstract class Participant implements Serializable {

	@Id 
	@GeneratedValue
	private int participantId;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private String gender;
	
	@ManyToMany(mappedBy="participants")
	private Collection<Movie> movies= new ArrayList<Movie>();
	private static final long serialVersionUID = 1L;

	public Participant() {
		super();
	}   
	
	public int getParticipantId() {
		return participantId;
	}
	public void setParticipantId(int participantId) {
		this.participantId = participantId;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}   
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}   
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}   
	
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}   
	
	
	public Collection<Movie> getMovies() {
		return movies;
	}
	public void setMovies(Collection<Movie> movies) {
		this.movies = movies;
	}
	
   
}

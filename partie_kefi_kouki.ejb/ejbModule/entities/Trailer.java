package entities;


import java.lang.String;

import javax.persistence.*;


@Entity

public class Trailer  {

	@Id
	private int trailerId;
	private String duration;
	String videoQuality;
	@ManyToOne
	private Movie movie;
	

	public Trailer() {
		super();
	}   
	
	public int getTrailerId() {
		return trailerId;
	}

	public void setTrailerId(int trailerId) {
		this.trailerId = trailerId;
	}

	public String getDuration() {
		return this.duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
   
}
